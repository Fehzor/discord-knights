/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bot;

import Gates.GateChannel;
import sx.blah.discord.api.events.Event;
import sx.blah.discord.api.events.IListener;
import sx.blah.discord.handle.impl.events.guild.channel.message.reaction.ReactionAddEvent;
import sx.blah.discord.handle.impl.events.guild.channel.message.reaction.ReactionRemoveEvent;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IMessage;

/**
 *
 * @author FF6EB4
 */
public class ReactionRemoveListener implements IListener<ReactionRemoveEvent>{
    public void handle(ReactionRemoveEvent event) {
        IMessage IM = event.getMessage();
        IChannel chan = IM.getChannel();
        
        GateChannel WC = GateChannel.get(chan);
        if(WC == null){
            return;
        }
        
        if(WC instanceof GateChannel){
            WC.removeReaction(event.getReaction());
        }
    }
}
