package Bot;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.ArrayList;
import sx.blah.discord.api.internal.json.objects.EmbedObject;
import sx.blah.discord.handle.impl.obj.ReactionEmoji;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.util.RequestBuffer;
import sx.blah.discord.util.RequestBuffer.RequestFuture;

/**
 *
 * @author FF6EB4
 */
public class MessageTuple {
    public String message;
    public EmbedObject embed;
    public IChannel sendTo;
    public boolean string = true;
    public ArrayList<ReactionEmoji> emotes = null;
    
    public MessageTuple(String mess, IChannel chan){
        this.message = mess;
        this.sendTo = chan;
    }
    
    public MessageTuple(EmbedObject mess, IChannel chan){
        this.embed = mess;
        this.sendTo = chan;
    }
    
    public MessageTuple(EmbedObject mess, IChannel chan, ArrayList<ReactionEmoji> emotes){
        this.embed = mess;
        this.sendTo = chan;
        this.emotes = emotes;
    }
    
    public boolean addOther(MessageTuple other){
        try{
            if(!this.sendTo.equals(other.sendTo)){
                return false;
            } else if(this.message.length() + other.message.length() > 2000){
                return false;
            } else {
                this.message+="\n"+other.message;
                return true;
            }
        } catch (Exception E){return false;}
    }
    
    public MessageTuple split(){
        
        int index = 1990;
        for(int i = 1800; i<1997; ++i){
            if(this.message.charAt(i)== '\n'){
                index = i;
            }
        }
        
        
        String text = this.message.substring(index);
        this.message = this.message.substring(0,index);
        MessageTuple ret = new MessageTuple(text,this.sendTo);
        return ret;
    }
    
    public boolean send() throws InterruptedException{
        Object test = null;
        
        if(message != null){
            test = RequestBuffer.request(() -> sendTo.sendMessage(message));
        }
        if(embed != null){
            test = RequestBuffer.request(() -> sendTo.sendMessage(embed));
        }
        
        //IMessage mess = sendTo.getMessageHistory().get(0);
        try{
            RequestFuture RF = (RequestFuture)test;
            
            IMessage mess = (IMessage)RF.get();
            
            //System.out.println(mess.getContent());
            if(emotes != null){
                for(ReactionEmoji RE : emotes){
                    Thread.sleep(250);

                    mess.addReaction(RE);
                }
            }
        } catch (Exception E){
            E.printStackTrace();
        }
        
        return true;
    }
}
