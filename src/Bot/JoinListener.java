/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bot;

import User.UserData;
import Gates.GateChannel;
import sx.blah.discord.api.events.Event;
import sx.blah.discord.api.events.IListener;
import sx.blah.discord.handle.impl.events.guild.member.UserJoinEvent;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IPrivateChannel;
import sx.blah.discord.handle.obj.IUser;

/**
 *
 * @author FF6EB4
 */
public class JoinListener implements IListener<UserJoinEvent>{
    public static final String JOIN_MESSAGE = "Hello and welcome!\n"
            + "\n"
            + "Chat in any gate to attack and activate your weapons! Be sure to ask plenty of questions and keep your wits about you!\n"
            + "\n"
            + "**Commands are as follows: **\n"
            + "*inv* = inventory (anywhere)\n"
            + "*gate* = view gate (inside of level)\n";
    
    public void handle(UserJoinEvent event) {
        IUser use = event.getUser();
        Launcher.PM(JOIN_MESSAGE,use.getLongID());
        
    }
}
