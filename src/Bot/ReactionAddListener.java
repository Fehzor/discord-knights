/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bot;

import User.UserData;
import Gates.GateChannel;
import sx.blah.discord.api.events.Event;
import sx.blah.discord.api.events.IListener;
import sx.blah.discord.handle.impl.events.guild.channel.message.reaction.ReactionAddEvent;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IPrivateChannel;

/**
 *
 * @author FF6EB4
 */
public class ReactionAddListener implements IListener<ReactionAddEvent>{
    public void handle(ReactionAddEvent event) {
        IMessage IM = event.getMessage();
        IChannel chan = IM.getChannel();
        
        if(chan.isPrivate()){
            IPrivateChannel IPC = (IPrivateChannel)chan;
            UserData UD = UserData.getUD(IPC.getRecipient());
            UD.invObj.react(event.getReaction());
        }
        
        GateChannel WC = GateChannel.get(chan);
        if(WC == null){
            return;
        }
        
        if(WC instanceof GateChannel){
            try{
                WC.addReaction(event.getReaction());
            } catch (Exception E){}
        }
    }
}
