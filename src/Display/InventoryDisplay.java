/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package Display;

import Bot.Launcher;
import User.UserData;
import Gates.GateMessage;
import com.vdurmont.emoji.Emoji;
import com.vdurmont.emoji.EmojiManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import sx.blah.discord.handle.impl.obj.ReactionEmoji;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IReaction;
import sx.blah.discord.handle.obj.IUser;

/**
 *
 * @author FF6EB4
 */

public class InventoryDisplay {
    public UserData user;
    IChannel chan;
    
    public ArrayList<String> items = new ArrayList<>();
    public HashMap<String,Integer> quantity;
    
    public HashMap<String,InventoryView> views = new HashMap<>();
    InventoryView current;

    
    public InventoryDisplay(UserData UD){
        this.user = UD;
        try {
            Thread.sleep(50);
        } catch (InterruptedException ex) {}
        IUser IU = Launcher.client.fetchUser(Long.parseLong(user.ID));
        this.chan = Launcher.client.getOrCreatePMChannel(IU);
        
        this.views.put("single", new SingleView(this));
        this.views.put("multi", new MultiView(this));
        this.views.put("stats", new StatsView(this));
        this.views.put("catalogue",new CatalogueView(this));
        this.views.put("order",new OrderView(this));
        this.views.put("alloy",new AlloyView(this));
        
        this.current = this.views.get("stats");
        
        this.update(UD);
    }
    
    public void update(UserData UD){
        quantity = UD.inventory.getData();
        items.clear();
        for(String S : quantity.keySet()){
            items.add(S);
        }
    }
    
    public void switchViews(String next){
        current = views.get(next);
        current.showView().send(chan);
    }
    
    public void topView(){
        if(current.equals(views.get("stats"))){
            current.showView().send(chan);
            return;
        }
        current = views.get("stats");
        current.showView().send(chan);
    }
    
    public void show(){
        this.current.showView().send(chan);
    }
    
    public void react(IReaction react){
        if(react.getUsers().size() == 1){
            return;
        }
        String name = EmojiManager.getByUnicode(react.getEmoji().getName()).getAliases().get(0);
        current.react(name);
    }
}


/*
public class Inventory {
    static final int PAGE_SIZE = 7;
    int start = 0;
    
    UserData user;
    IUser IU;
    
    WorldMessage currentWM;
    IChannel PMChannel;
    
    Item previewing = null;
    
    ArrayList<String> items;
    HashMap<String,Integer> quantity;
    
    //:smile: 
    
    public static final ReactionEmoji YOU = ReactionEmoji.of(EmojiManager.getForAlias("smile").getUnicode());
    
    public static final ReactionEmoji BACK = ReactionEmoji.of(EmojiManager.getForAlias("leftwards_arrow_with_hook").getUnicode());
    public static final ReactionEmoji PAGE_NEXT = ReactionEmoji.of(EmojiManager.getForAlias("arrow_right").getUnicode());
    public static final ReactionEmoji PAGE_BACK = ReactionEmoji.of(EmojiManager.getForAlias("arrow_left").getUnicode());
    
    public static final ReactionEmoji EQUIP = ReactionEmoji.of(EmojiManager.getForAlias("crossed_swords").getUnicode());
    public static final ReactionEmoji SLOT_1 = ReactionEmoji.of(EmojiManager.getForAlias("one").getUnicode());
    public static final ReactionEmoji SLOT_2 = ReactionEmoji.of(EmojiManager.getForAlias("two").getUnicode());
    public static final ReactionEmoji SLOT_3 = ReactionEmoji.of(EmojiManager.getForAlias("three").getUnicode());
    public static final ReactionEmoji FOUR = ReactionEmoji.of(EmojiManager.getForAlias("four").getUnicode());
    public static final ReactionEmoji FIVE = ReactionEmoji.of(EmojiManager.getForAlias("five").getUnicode());
    public static final ReactionEmoji SIX = ReactionEmoji.of(EmojiManager.getForAlias("six").getUnicode());
    public static final ReactionEmoji SEVEN = ReactionEmoji.of(EmojiManager.getForAlias("seven").getUnicode());
    
    public static final ReactionEmoji EAT = ReactionEmoji.of(EmojiManager.getForAlias("fork_knife_plate").getUnicode());
    
    public Inventory(UserData UD){
        user = UD;
        IU = Launcher.client.fetchUser(Long.parseLong(user.ID));
        try {
            Thread.sleep(50);
        } catch (InterruptedException ex) {}
        IChannel make = Launcher.client.getOrCreatePMChannel(IU);
        PMChannel = make;
        
        update(UD);
        //reset();
    }
    
    public void update(UserData UD){
        quantity = UD.inventory.getData();
        items = new ArrayList<>();
        for(String S : quantity.keySet()){
            items.add(S);
        }
    }
    
    public void choose(IReaction react){
        String name = EmojiManager.getByUnicode(react.getEmoji().getName()).getAliases().get(0);
        
        if(name.equals("fork_knife_plate")){
            if(previewing != null){
                if(user.take(previewing.name)){
                    
                    Stats make = previewing.stats.addStats(previewing.brand.stats);
                    
                    Stats.giveEXPtoUD(user,make.life,LIFE_FORCE);
                    Stats.giveEXPtoUD(user,make.phys,PHYSICAL);
                    Stats.giveEXPtoUD(user,make.endu,ENDURANCE);
                    Stats.giveEXPtoUD(user,make.empa,EMPATHY);
                    Stats.giveEXPtoUD(user,make.luck,LUCK);
                    Stats.giveEXPtoUD(user,make.libi,LIBIDO);
                    Stats.giveEXPtoUD(user,make.hung,HUNGER);
                    
                    new WorldMessage("OM NOM NOM",previewing.nom).send(this.PMChannel);
                } else {
                    new WorldMessage("WAIT!","You don't have that!").send(this.PMChannel);
                }
            }
        } else if(name.equals("smile")){
            this.youView();
        } else if(name.equals("leftwards_arrow_with_hook")){
            //this.start = 0;
            this.reset();
        } else if(name.equals("arrow_right")){
            this.start += PAGE_SIZE;
            if(this.start > user.inventory.getData().keySet().size()){
                this.start = 0;
            }
            this.reset();
        } else if(name.equals("arrow_left")){
            this.start -= PAGE_SIZE;
            if(this.start<0){
                this.start = user.inventory.getData().keySet().size() - user.inventory.getData().keySet().size() % PAGE_SIZE;
            }
            this.reset();
        } else if(name.equals("crossed_swords")){
            if(previewing != null){
                boolean worked = user.take(previewing.name);
                
                if(worked){
                    user.give(user.equipped.getData());
                    user.equipped.writeData(previewing.name);
                    reset();
                } else {
                    new WorldMessage("WAIT!","You don't have that!").send(this.PMChannel);
                }
                
            }
        } else if(name.equals("one")){
            if(previewing != null){
                boolean worked = user.take(previewing.name);
                
                if(worked){
                    user.give(user.toolA.getData());
                    user.toolA.writeData(previewing.name);
                    new WorldMessage("SUCCESS!","Item selection complete!").send(this.PMChannel);
                } else {
                    new WorldMessage("WAIT!","You don't have that!").send(this.PMChannel);
                }
            } else {
                String itemName = getOption(0);
                System.out.println("ITEM NAME IS "+itemName);
                this.showItem(itemName);
            }
        } else if(name.equals("two")){
            if(previewing != null){
                boolean worked = user.take(previewing.name);
                
                if(worked){
                    user.give(user.toolB.getData());
                    user.toolB.writeData(previewing.name);
                    new WorldMessage("SUCCESS!","Item selection complete!").send(this.PMChannel);
                } else {
                    new WorldMessage("WAIT!","You don't have that!").send(this.PMChannel);
                }
            } else {
                String itemName = getOption(1);
                
                this.showItem(itemName);
            }
        } else if(name.equals("three")){
            if(previewing != null){
                boolean worked = user.take(previewing.name);
                
                if(worked){
                    user.give(user.toolC.getData());
                    user.toolC.writeData(previewing.name);
                    new WorldMessage("SUCCESS!","Item selection complete!").send(this.PMChannel);
                } else {
                    new WorldMessage("WAIT!","You don't have that!").send(this.PMChannel);
                }
            } else {
                String itemName = getOption(2);
                
                this.showItem(itemName);
            }
        } else if(name.equals("four")){
            String itemName = getOption(3);

            this.showItem(itemName);
        } else if(name.equals("five")){
            String itemName = getOption(4);

            this.showItem(itemName);
        } else if(name.equals("six")){
            String itemName = getOption(5);

            this.showItem(itemName);
        } else if(name.equals("seven")){
            String itemName = getOption(6);

            this.showItem(itemName);
        }
    }
    
    public void reset(){
        this.previewing = null;
        currentWM = new WorldMessage("Inventory",this.showInv(start, PAGE_SIZE));
        
        sendCurrent();
        
        this.currentWM.addEmoji(YOU);
        this.currentWM.addEmoji(PAGE_BACK);
        
        int index = 0;
        int whichToSend = 1;
        for(String S : user.inventory.getData().keySet()){
            if(index >= start && index < start+PAGE_SIZE){
                if(whichToSend==1){
                    this.currentWM.addEmoji(SLOT_1);
                    whichToSend++;
                } else if(whichToSend==2){
                    this.currentWM.addEmoji(SLOT_2);
                    whichToSend++;
                } else if(whichToSend==3){
                    this.currentWM.addEmoji(SLOT_3);
                    whichToSend++;
                } else if(whichToSend==4){
                    this.currentWM.addEmoji(FOUR);
                    whichToSend++;
                } else if(whichToSend==5){
                    this.currentWM.addEmoji(FIVE);
                    whichToSend++;
                } else if(whichToSend==6){
                    this.currentWM.addEmoji(SIX);
                    whichToSend++;
                } else if(whichToSend==7){
                    this.currentWM.addEmoji(SEVEN);
                    whichToSend++;
                }
            }
            index++;
        }
        
        this.currentWM.addEmoji(PAGE_NEXT);
    }
    
    public String getOption(int op){
        int index = 0;
        for(String S : user.inventory.getData().keySet()){
            if(index == start + op){
                return S;
            }
            index++;
        }
        return "NULL";
    }
    
    public void showItem(String name){

        this.previewing = Item.getItem(name);
        this.currentWM = new WorldMessage("**"+previewing+"**",previewing.longhand());
        
        sendCurrent();
        
        this.currentWM.addEmoji(BACK);
        this.currentWM.addEmoji(EQUIP);
        this.currentWM.addEmoji(SLOT_1);
        this.currentWM.addEmoji(SLOT_2);
        this.currentWM.addEmoji(SLOT_3);
        this.currentWM.addEmoji(EAT);
    }
    
    public void youView(){
        String disp = "";
        Item wep = Item.getItem(user.equipped.getData());
        disp+=EmojiManager.getForAlias("crossed_swords").getUnicode()+" equipped as weapon: "+wep+"\n";
        wep = Item.getItem(user.toolA.getData());
        disp+=EmojiManager.getForAlias("one").getUnicode()+" equipped in slot 1: "+wep+"\n";
        wep = Item.getItem(user.toolB.getData());
        disp+=EmojiManager.getForAlias("two").getUnicode()+" equipped in slot 2: "+wep+"\n";
        wep = Item.getItem(user.toolC.getData());
        disp+=EmojiManager.getForAlias("three").getUnicode()+" equipped in slot 3: "+wep+"\n";
        
        this.currentWM = new WorldMessage("Your current equiped items-",disp);
        this.currentWM.send(PMChannel);
        this.currentWM.addEmoji(BACK);
    }
    
    public void react(IReaction react){
        if(react.getUsers().size() == 1){
            return;
        }
        
        this.choose(react);
    }
    
    public void sendCurrent(){
        if(IU.isBot()){
            return;
        }
        currentWM.send(PMChannel);
    }
    
    public String showInv(int start, int till){
        String s = "";
        
        int index = 0;
        
        int inds = 0;
        for(String S : items){
            if(index >= start && index < start+till){
                
                try{
                    Item itm = Item.getItem(S);
                    
                    if(quantity.get(S) > 1){
                        inds++;
                        s+="**ITEM #"+inds+":** "+itm.toString()+" ("+quantity.get(S)+")\n";
                    } else {
                        inds++;
                        s+="**ITEM #"+inds+":** "+itm.toString()+"\n";
                    }
                    
                } catch (Exception E){
                    E.printStackTrace();
                }
            }
            index++;
        }
        
        return s;
    }
}
*/