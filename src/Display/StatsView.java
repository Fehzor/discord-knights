/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Display;

import Gates.GateMessage;
import Display.InventoryButton;
import Display.InventoryDisplay;
import Display.InventoryView;
import Item.Structure.Material;
import Item.Structure.Mineral;
import com.vdurmont.emoji.EmojiManager;
import java.util.HashMap;

/**
 *
 * @author FF6EB
 */
public class StatsView extends InventoryView{
    
    public static final int FIELD_LENGTH = 16;
    
    public StatsView(InventoryDisplay inv){
        super(inv);
        
        InventoryButton gotoItemView = new InventoryButton("baggage_claim"){
            public void buttonPressed(){
                goItems();
            }
        };
        addButton(gotoItemView);
        
        InventoryButton gotoCatalogueView = new InventoryButton("repeat"){
            public void buttonPressed(){
                goCatalogue();
            }
        };
        addButton(gotoCatalogueView);
        //:: 
        
        InventoryButton gotoAlloyView = new InventoryButton("pick"){
            public void buttonPressed(){
                goAlloy();
            }
        };
        addButton(gotoAlloyView);
        //:calendar_spiral: 
    }
    
    public GateMessage showView(){
        String disp = "\n**Currency**\n";
        
        disp+="Crowns: "+inventory.user.crowns.getData()+"\n\n";
        
        HashMap<String, Integer> minmap = inventory.user.minerals.getData();
        String minerals = "";
        for(String key : minmap.keySet()){
            Mineral M = Mineral.getMineral(key);
            
            minerals+=M+": "+minmap.get(key)+"\n";
        }
        disp+="**MINERALS**\n";
        disp+=minerals+"\n";
        
        HashMap<String, Integer> matmap = inventory.user.materials.getData();
        String materials = "```";
        String line = "";
        boolean two = false;
        for(String key : matmap.keySet()){
            Material M = Material.getMaterial(key);
            
            line+=M.name+": "+matmap.get(key);
            if(!two){
                while (line.length() % FIELD_LENGTH != 0){
                    line+=" ";
                }
            }
            
            materials+=line;
            line="";
            
            if(two){
                materials+="\n";
            }
            
            two = !two;
        }
        disp+="**MATERIALS**\n";
        disp+=materials+"```\n";
        
        disp+="**EQUIPMENT**\n";
        disp += "Weapon A: "+inventory.user.wepA.getData()+"\n";
        disp += "Weapon B: "+inventory.user.wepB.getData()+"\n";
        disp += "Weapon C: "+inventory.user.wepC.getData()+"\n";
        
        
        disp += "\n**BUTTONS**\n";
        disp += EmojiManager.getForAlias("baggage_claim").getUnicode()+" View Gear\n";
        disp += EmojiManager.getForAlias("repeat").getUnicode()+" Craft Gear\n";
        disp += EmojiManager.getForAlias("pick").getUnicode()+" Craft Alloys\n";
        
        GateMessage ret = new GateMessage("**OVERVIEW**",disp);
        addButtons(ret);
        
        return ret;
    }
    
    private void goItems(){
        inventory.switchViews("multi");
    }
    
    private void goAlloy(){
        ((AlloyView)inventory.views.get("alloy")).setItems();
        inventory.switchViews("alloy");
    }
    
    private void goCatalogue(){
        ((CatalogueView)inventory.views.get("catalogue")).setItems();
        inventory.switchViews("catalogue");
    }
}
