/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Display;


import Gates.GateMessage;
import Item.Craft;
import Item.Parser.ItemEffect;
import Item.Structure.CraftNode;
import com.vdurmont.emoji.EmojiManager;
import sx.blah.discord.handle.impl.obj.ReactionEmoji;

/**
 *
 * @author FF6EB4
 */
public class OrderView extends InventoryView{
    public CraftNode item;
    
    public OrderView(InventoryDisplay inv){
        super(inv);
        
        InventoryButton gotoItemView = new InventoryButton("leftwards_arrow_with_hook"){
            public void buttonPressed(){
                goItems();
            }
        };

        InventoryButton craft = new InventoryButton("hammer"){
            public void buttonPressed(){
                craft();
            }
        };
        
        addButton(gotoItemView);
        addButton(craft);
        
    }
    
    public GateMessage showView(){
        
        String vu = item.toString()+"\n";
        
        if(!item.pre.equals("None")){
            vu += EmojiManager.getForAlias("hammer").getUnicode()+"Craft this item now! (If applicable..)\n";
        } else {
            vu += EmojiManager.getForAlias("hammer").getUnicode()+"THIS ITEM IS UNCRAFTABLE\n";
        }
        vu +="*You can also use the craft command to craft on the fly- craft item name*";
        
        GateMessage ret = new GateMessage("ITEM PREVIEW",vu);
        
        
        
        addButtons(ret);
        
        return ret;
    }
    
    private void goItems(){
        ((CatalogueView)inventory.views.get("catalogue")).setItems();
        inventory.switchViews("catalogue");
    }
    
    private void craft(){
        String disp = Craft.create(inventory.user, item.name);
        new GateMessage("CRAFTING.........",disp).send(inventory.chan);
    }
}
