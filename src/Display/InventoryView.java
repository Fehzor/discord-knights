/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Display;

import Gates.GateMessage;
import com.vdurmont.emoji.EmojiManager;
import java.util.ArrayList;

/**
 *
 * @author FF6EB4
 */
public class InventoryView {
    public ArrayList<InventoryButton> buttons = new ArrayList<>();
    
    InventoryDisplay inventory;
    public InventoryView(InventoryDisplay inv){
        inventory = inv;
    }
    
    public void addButton(InventoryButton add){
        buttons.add(add);
    }
    
    public GateMessage showView(){
        return new GateMessage("DEFAULT_INVENTORY_VIEW","PUT CODE HERE LOL");
    }
    
    public void addButtons(GateMessage WM){
        for(InventoryButton B : buttons){
            WM.addEmoji(B.emoji);
        }
    }
    
    public void react(String name){
        for(InventoryButton B : buttons){
            String compare = B.name;
            if(compare.equals(name)){
                B.buttonPressed();
                return;
            }
        }
        System.err.println("BUTTON NOT FOUND.");
    }
}
