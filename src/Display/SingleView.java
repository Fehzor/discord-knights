/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Display;


import Gates.GateMessage;
import Item.Parser.ItemEffect;
import Item.Structure.CraftNode;
import com.vdurmont.emoji.EmojiManager;
import sx.blah.discord.handle.impl.obj.ReactionEmoji;

/**
 *
 * @author FF6EB4
 */
public class SingleView extends InventoryView{
    public CraftNode item;
    
    public SingleView(InventoryDisplay inv){
        super(inv);
        
        InventoryButton gotoItemView = new InventoryButton("leftwards_arrow_with_hook"){
            public void buttonPressed(){
                goItems();
            }
        };

        InventoryButton equipA = new InventoryButton("one"){
            public void buttonPressed(){
                equip(1);
            }
        };
        InventoryButton equipB = new InventoryButton("two"){
            public void buttonPressed(){
                equip(2);
            }
        };
        InventoryButton equipC = new InventoryButton("three"){
            public void buttonPressed(){
                equip(3);
            }
        };
        
        InventoryButton eats = new InventoryButton("fire"){
            public void buttonPressed(){
                eat();
            }
        };
        
        InventoryButton eatsAll = new InventoryButton("radioactive"){
            public void buttonPressed(){
                eatAll();
            }
        };
        
        addButton(gotoItemView);
        addButton(equipA);
        addButton(equipB);
        addButton(equipC);
        addButton(eats);
        addButton(eatsAll);
        
    }
    
    public GateMessage showView(){
        
        String vu = item.toString()+"\n\n";
        vu += EmojiManager.getForAlias("one").getUnicode()+"Equip to slot A\n";
        vu += EmojiManager.getForAlias("two").getUnicode()+"Equip to slot B\n";
        vu += EmojiManager.getForAlias("three").getUnicode()+"Equip to slot C\n";
        vu += EmojiManager.getForAlias("fire").getUnicode()+"Discard one\n";
        vu += EmojiManager.getForAlias("radioactive").getUnicode()+"Discard all\n";
        vu += "*You can also use equip 1/2/3 name of weapon to equip quickly...*\n";
        
        GateMessage ret = new GateMessage("ITEM",vu);
        
        addButtons(ret);
        
        return ret;
    }
    
    private void goItems(){
        inventory.switchViews("multi");
    }
    
    public void equip(int slot){
        boolean worked = inventory.user.take(item.name);
                
        if(worked && slot == 1){
            inventory.user.give(inventory.user.wepA.getData());
            inventory.user.wepA.writeData(item.name);
            new GateMessage("SUCCESS!","Item selection complete!").send(inventory.chan);
        } else if(worked && slot == 2){
            inventory.user.give(inventory.user.wepB.getData());
            inventory.user.wepB.writeData(item.name);
            new GateMessage("SUCCESS!","Item selection complete!").send(inventory.chan);
        } else if(worked && slot == 3){
            inventory.user.give(inventory.user.wepC.getData());
            inventory.user.wepC.writeData(item.name);
            new GateMessage("SUCCESS!","Item selection complete!").send(inventory.chan);
        } else {
            new GateMessage("WAIT!","You don't have that!").send(inventory.chan);
        }
    }
    
    public void equip(CraftNode item, int slot){
        boolean worked = inventory.user.inventory.getData().containsKey(item.name);
                
        if(worked && slot == 1){
            inventory.user.take(item.name);
            inventory.user.give(inventory.user.wepA.getData());
            inventory.user.wepA.writeData(item.name);
            new GateMessage("SUCCESS!","Item selection complete!").send(inventory.chan);
        } else if(worked && slot == 2){
            inventory.user.take(item.name);
            inventory.user.give(inventory.user.wepB.getData());
            inventory.user.wepB.writeData(item.name);
            new GateMessage("SUCCESS!","Item selection complete!").send(inventory.chan);
        } else if(worked && slot == 3){
            inventory.user.take(item.name);
            inventory.user.give(inventory.user.wepC.getData());
            inventory.user.wepC.writeData(item.name);
            new GateMessage("SUCCESS!","Item selection complete!").send(inventory.chan);
        } else {
            new GateMessage("WAIT!","You don't have that! Or you chose the bad slot..").send(inventory.chan);
        }
    }
    
    public void eat(){
        inventory.user.take(item.name);
        
        inventory.update(inventory.user);
    }
    
    public void eatAll(){
        while(inventory.user.take(item.name));
        
        inventory.update(inventory.user);
    }
}
