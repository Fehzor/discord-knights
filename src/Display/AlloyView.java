/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Display;

import Gates.GateMessage;
import Display.InventoryButton;
import Display.InventoryDisplay;
import Display.InventoryView;
import Item.Structure.CraftNode;
import Item.Structure.CraftPath;
import Item.Structure.Mineral;
import static Item.Structure.Mineral.ALLOY_WEIGHT;
import com.vdurmont.emoji.EmojiManager;
import java.util.ArrayList;
import org.apache.commons.lang3.text.WordUtils;

/**
 *
 * @author FF6EB
 */
public class AlloyView extends InventoryView{
    int size = 7;
    int start = 0;
    
    public ArrayList<String> alloys = new ArrayList<>();
    
    public ArrayList<InventoryButton> numberButtons = new ArrayList<>();
    
    public AlloyView(InventoryDisplay inv){
        super(inv);
        
        InventoryButton gotoEquips = new InventoryButton("leftwards_arrow_with_hook"){
            public void buttonPressed(){
                goBack();
            }
        };
        
        InventoryButton gotoItemView1 = new InventoryButton("one"){
            public void buttonPressed(){
                craftNum(1);
            }
        };
        InventoryButton gotoItemView2 = new InventoryButton("two"){
            public void buttonPressed(){
                craftNum(2);
            }
        };
        InventoryButton gotoItemView3 = new InventoryButton("three"){
            public void buttonPressed(){
                craftNum(3);
            }
        };
        InventoryButton gotoItemView4 = new InventoryButton("four"){
            public void buttonPressed(){
                craftNum(4);
            }
        };
        InventoryButton gotoItemView5 = new InventoryButton("five"){
            public void buttonPressed(){
                craftNum(5);
            }
        };
        InventoryButton gotoItemView6 = new InventoryButton("six"){
            public void buttonPressed(){
                craftNum(6);
            }
        };
        InventoryButton gotoItemView7 = new InventoryButton("seven"){
            public void buttonPressed(){
                craftNum(7);
            }
        };
        
        InventoryButton pageLeft = new InventoryButton("arrow_left"){
            public void buttonPressed(){
                goLeft();
            }
        };
        
        InventoryButton pageRight = new InventoryButton("arrow_right"){
            public void buttonPressed(){
                goRight();
            }
        };
        
        addButton(gotoEquips);
        
        this.numberButtons.add(gotoItemView1);
        this.numberButtons.add(gotoItemView2);
        this.numberButtons.add(gotoItemView3);
        this.numberButtons.add(gotoItemView4);
        this.numberButtons.add(gotoItemView5);
        this.numberButtons.add(gotoItemView6);
        this.numberButtons.add(gotoItemView7);
        
        addButton(pageLeft);
        addButton(pageRight);
        
        addButton(gotoItemView1);
        addButton(gotoItemView2);
        addButton(gotoItemView3);
        addButton(gotoItemView4);
        addButton(gotoItemView5);
        addButton(gotoItemView6);
        addButton(gotoItemView7);
    }
    
    public void addButtons(GateMessage WM){
        WM.addEmoji(buttons.get(0).emoji);
        if(start >= size){
            WM.addEmoji(buttons.get(1).emoji);
        }
        int numButs = alloys.size() - start;
        if(numButs > size)numButs = size;
        for(int i = 0; i < numButs; ++i){
            WM.addEmoji(numberButtons.get(i).emoji);
        }
        if(alloys.size() - start > size){
            WM.addEmoji(buttons.get(2).emoji);
        }
    }
    
    public GateMessage showView(){
        String disp = showInv(start, start+size);
        
        //disp +="\n*You can also view any item, even those you do not have, via the view command- view item name*\n";
        
        GateMessage ret = new GateMessage("Alloys you can craft: ",disp);
        
        
        
        addButtons(ret);
        
        return ret;
    }
    
    private void goBack(){
        inventory.switchViews("stats");
    }
    
    private void goLeft(){
        start -= size;
        if(start < 0)start=0;
        this.showView().send(inventory.chan);
    }
    
    private void goRight(){
        start += size;
        while(start >= alloys.size())start-=size;
        this.showView().send(inventory.chan);
    }
    
    private String [] alloyCost(String S){
        String [] cost = new String[2];
        cost[0]="None";
        cost[1]="None";
        
        switch(S){
            case "Beast Alloy":
                cost[0] = "Red";
                cost[1] = "Green";
                break;
            case "Slime Alloy":
                cost[0] = "Yellow";
                cost[1] = "Green";
                break;
            case "Gremlin Alloy":
                cost[0] = "Red";
                cost[1] = "Blue";
                break;
            case "Construct Alloy":
                cost[0] = "Blue";
                cost[1] = "Yellow";
                break;
            case "Fiend Alloy":
                cost[0] = "Red";
                cost[1] = "Purple";
                break;
            case "Undead Alloy":
                cost[0] = "Purple";
                cost[1] = "Yellow";
                break;
            case "Fire Alloy":
                cost[0] = "Red";
                cost[1] = "Yellow";
                break;
            case "Poison Alloy":
                cost[0] = "Green";
                cost[1] = "Purple";
                break;
            case "Freeze Alloy":
                cost[0] = "Green";
                cost[1] = "Blue";
                break;
            case "Shock Alloy":
                cost[0] = "Blue";
                cost[1] = "Purple";
                break;
            case "Arena Alloy":
                cost = new String[5];
                cost[0] = "Red";
                cost[1] = "Green";
                cost[2] = "Blue";
                cost[3] = "Yellow";
                cost[4] = "Purple";
                break;
        }
        
        return cost;
    }
    
    public void craft(String name){
        
        String [] cost = alloyCost(name);
        
        if(cost[0].equals("None") || !alloys.contains(name)){
            new GateMessage("CRAFTING","FAILURE! Unable to find item desired!").send(inventory.chan);
            return;
        }
        
        for(String A : cost){
            if(!inventory.user.minerals.getData().containsKey(A)){
                new GateMessage("CRAFTING","BAD... BAD.... No "+A).send(inventory.chan);
                return;
            }
            if(inventory.user.minerals.getData().get(A) < ALLOY_WEIGHT){
                new GateMessage("CRAFTING","BAD... BAD.... No "+A).send(inventory.chan);
                return;
            }
        }
        for(String A : cost){
            inventory.user.minerals.getData().put(A, inventory.user.minerals.getData().get(A) - ALLOY_WEIGHT);
        }
        
        inventory.user.giveMinerals(name, 1);
        
        
        new GateMessage("CRAFTING","GOOOOO").send(inventory.chan);
    }
    
    private void craftNum(int num){
        num = num - 1;
        Mineral get = Mineral.getMineral(alloys.get(start + num));
        
        craft(get.name);
    }
    
    private String showInv(int start, int till){
        if(alloys.size() == 0){
            return "No recipes found.";
        }
        
        String s = "";
        
        int index = 0;
        
        int inds = 0;
        for(String S : alloys){
            if(index >= start && index < start+till){
                try{
                    inds++;
                    s+="**ALLOY #"+inds+":** "+S+"\n";
                    s+="Cost: ";
                    for(String cost : alloyCost(S)){
                        s+=+ALLOY_WEIGHT+" "+cost+"; ";
                    }
                    s+="\n";
                } catch (Exception E){
                    E.printStackTrace();
                }
            }
            index++;
        }
        
        return s;
    }
    
    public void setItems(){
        alloys.clear();
        for(String recipe : inventory.user.recipes.getData()){
            if(recipe.contains("Alloy")){
                alloys.add(recipe);
            }
        }
    }
}
