/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Display;

import Gates.GateMessage;
import Display.InventoryButton;
import Display.InventoryDisplay;
import Display.InventoryView;
import Item.Structure.Material;
import Item.Structure.Mineral;
import com.vdurmont.emoji.EmojiManager;
import java.util.HashMap;

/**
 *
 * @author FF6EB
 */
public class MissionView extends InventoryView{
    
    public static final int FIELD_LENGTH = 16;
    
    public MissionView(InventoryDisplay inv){
        super(inv);
        
        
        
    }
    
    public GateMessage showView(){
        String disp = "\n**MISSIONS**\n";
        
        disp += "\n**BUTTONS**\n";
        
        GateMessage ret = new GateMessage("**OVERVIEW**",disp);
        //addButtons(ret);
        
        return ret;
    }
}
