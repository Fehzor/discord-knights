/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Display;

import Gates.GateMessage;
import Display.InventoryButton;
import Display.InventoryDisplay;
import Display.InventoryView;
import Item.Structure.CraftNode;
import Item.Structure.CraftPath;
import com.vdurmont.emoji.EmojiManager;
import java.util.ArrayList;

/**
 *
 * @author FF6EB
 */
public class CatalogueView extends InventoryView{
    int size = 7;
    int start = 0;
    
    public ArrayList<String> items = new ArrayList<>();
    
    public ArrayList<InventoryButton> numberButtons = new ArrayList<>();
    
    public CatalogueView(InventoryDisplay inv){
        super(inv);
        
        InventoryButton gotoEquips = new InventoryButton("leftwards_arrow_with_hook"){
            public void buttonPressed(){
                goBack();
            }
        };
        
        InventoryButton gotoItemView1 = new InventoryButton("one"){
            public void buttonPressed(){
                goItem(1);
            }
        };
        InventoryButton gotoItemView2 = new InventoryButton("two"){
            public void buttonPressed(){
                goItem(2);
            }
        };
        InventoryButton gotoItemView3 = new InventoryButton("three"){
            public void buttonPressed(){
                goItem(3);
            }
        };
        InventoryButton gotoItemView4 = new InventoryButton("four"){
            public void buttonPressed(){
                goItem(4);
            }
        };
        InventoryButton gotoItemView5 = new InventoryButton("five"){
            public void buttonPressed(){
                goItem(5);
            }
        };
        InventoryButton gotoItemView6 = new InventoryButton("six"){
            public void buttonPressed(){
                goItem(6);
            }
        };
        InventoryButton gotoItemView7 = new InventoryButton("seven"){
            public void buttonPressed(){
                goItem(7);
            }
        };
        
        InventoryButton pageLeft = new InventoryButton("arrow_left"){
            public void buttonPressed(){
                goLeft();
            }
        };
        
        InventoryButton pageRight = new InventoryButton("arrow_right"){
            public void buttonPressed(){
                goRight();
            }
        };
        
        addButton(gotoEquips);
        
        this.numberButtons.add(gotoItemView1);
        this.numberButtons.add(gotoItemView2);
        this.numberButtons.add(gotoItemView3);
        this.numberButtons.add(gotoItemView4);
        this.numberButtons.add(gotoItemView5);
        this.numberButtons.add(gotoItemView6);
        this.numberButtons.add(gotoItemView7);
        
        addButton(pageLeft);
        addButton(pageRight);
        
        addButton(gotoItemView1);
        addButton(gotoItemView2);
        addButton(gotoItemView3);
        addButton(gotoItemView4);
        addButton(gotoItemView5);
        addButton(gotoItemView6);
        addButton(gotoItemView7);
    }
    
    public void addButtons(GateMessage WM){
        WM.addEmoji(buttons.get(0).emoji);
        if(start >= size){
            WM.addEmoji(buttons.get(1).emoji);
        }
        int numButs = items.size() - start;
        if(numButs > size)numButs = size;
        for(int i = 0; i < numButs; ++i){
            WM.addEmoji(numberButtons.get(i).emoji);
        }
        if(items.size() - start > size){
            WM.addEmoji(buttons.get(2).emoji);
        }
    }
    
    public GateMessage showView(){
        String disp = showInv(start, start+size);
        
        disp +="\n*You can also view any item, even those you do not have, via the view command- view item name*\n";
        
        GateMessage ret = new GateMessage("Weapons you can craft: ",disp);
        
        
        
        addButtons(ret);
        
        return ret;
    }
    
    private void goBack(){
        inventory.switchViews("stats");
    }
    
    private void goLeft(){
        start -= size;
        if(start < 0)start=0;
        this.showView().send(inventory.chan);
    }
    
    private void goRight(){
        start += size;
        while(start >= items.size())start-=size;
        this.showView().send(inventory.chan);
    }
    
    private void goItem(int num){
        num = num - 1;
        CraftNode set = CraftNode.getNode(items.get(start + num));
        ((OrderView)inventory.views.get("order")).item = set;
        inventory.switchViews("order");
    }
    
    private String showInv(int start, int till){
        if(items.size() == 0){
            return "No items found. Go craft something?";
        }
        
        String s = "";
        
        int index = 0;
        
        int inds = 0;
        for(String S : items){
            if(index >= start && index < start+till){
                
                try{
                    CraftNode itm = CraftNode.getNode(S);
                    inds++;
                    s+="**ITEM #"+inds+":** "+itm.toString()+"\n";
                } catch (Exception E){
                    E.printStackTrace();
                }
            }
            index++;
        }
        
        return s;
    }
    
    public void setItems(){
        items.clear();
        CraftPath.setupCraftables(items, inventory.user);
    }
}
