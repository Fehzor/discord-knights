/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Display;

import Gates.GateMessage;
import com.vdurmont.emoji.EmojiManager;
import sx.blah.discord.handle.impl.obj.ReactionEmoji;

/**
 *
 * @author FF6EB4
 */
public class InventoryButton {
    ReactionEmoji emoji;
    String name;
    
    public InventoryButton(String emoji){
        this.name = emoji;
        this.emoji = ReactionEmoji.of(EmojiManager.getForAlias(emoji).getUnicode());
    }
    
    public void buttonPressed(){}
    
    public void add(GateMessage WM){
        WM.addEmoji(this.emoji);
    }
}
