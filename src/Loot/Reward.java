/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Loot;

import User.UserData;
import java.util.HashMap;

/**
 *
 * @author FF6EB
 */
public class Reward {
    public long crowns = 0;
    public HashMap<String, Integer> minerals = new HashMap<>();
    public HashMap<String, Integer> materials = new HashMap<>();
    public String recipe = "None";
    public String gear = "None";
    
    public Reward(){}
    
    
    
    public String give(UserData UD){
        String rec = "";
        
        if(!gear.equals("None")){
            UD.give(gear);
            rec+="The item "+gear+"! \n";
        }
        if(!recipe.equals("None")){
            UD.addRecipe(recipe);
            rec+="The recipe for "+recipe+"! \n";
        }
        
        UD.crowns.append(this.crowns);
        if(crowns > 0){
            rec+=crowns+" crowns.. \n";
        }
        
        for(String min : minerals.keySet()){
            UD.giveMinerals(min, minerals.get(min));
            rec+=minerals.get(min)+" "+min+" minerals.. \n";
        }
        for(String mat : materials.keySet()){
            UD.giveMaterials(mat, materials.get(mat));
            rec+=materials.get(mat)+" "+mat+" materials.. \n";
        }
        
        if(rec.equals("")){
            rec+="NOTHING. GOOD DAY SIR.";
        }
        
        return " received... \n"+rec;
    }
}
