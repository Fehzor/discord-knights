/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Loot;

import java.util.HashMap;
import java.util.Random;

/**
 *
 * @author FF6EB4
 */
public class SuperRandom {
    public static Random oRan = new Random();
    
    public static int getWeighted(HashMap<Integer,Integer> tally){
        int total = 0;
        for(int key : tally.keySet()){
            total+= tally.get(key);
        }
        
        total = oRan.nextInt(total);
        
        for(int key : tally.keySet()){
            total -= tally.get(key);
            
            if(total <= 0){
                return key;
            }
        }
        
        return -1;
    }
}
