/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gates;

import Bot.Launcher;
import java.awt.Color;
import java.util.ArrayList;
import sx.blah.discord.handle.impl.obj.ReactionEmoji;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.util.EmbedBuilder;

/**
 *
 * @author andrewwhitmer
 */
public class GateMessage {
    Color C = Color.GRAY;
    
    String header = "MESSAGE";
    
    ArrayList<String>body = new ArrayList<>();
    
    ArrayList<ReactionEmoji> emotes = new ArrayList<>();
    
    public GateMessage(String header, String desc){
        this.header = header;
        this.body.add(desc);
    }
    
    public void append(GateMessage other){
        String add = "**"+other.header+"**";
        
        body.add(add);
        
        for(String S : other.body){
            this.body.add(S);
        }
    }
    
    public void append(String other){
        body.add(other);
    }
    
    public void addEmoji(ReactionEmoji add){
        this.emotes.add(add);
    }
    
    public void send(IChannel chan){
        try{
           EmbedBuilder builder = new EmbedBuilder();

           builder.withColor((int)C.getRed(),(int) C.getGreen(),(int) C.getBlue());
         
           builder.withTitle(header);

           for (String S : body){
               builder.appendDescription(S+"\n");
           }

           Launcher.send(builder, chan, emotes);
       } catch(Exception E){E.printStackTrace();}
    }
}
