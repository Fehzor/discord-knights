/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gates;

import Bot.Launcher;
import Item.Parser.EffectParser;
import Item.Structure.CraftNode;
import User.UserData;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import sx.blah.discord.handle.obj.IChannel;

/**
 *
 * @author FF6EB
 */
public class ActivityLog extends Thread{
    public static ArrayList<UserData> activeUsers = new ArrayList<>();
    public static HashMap<UserData, IChannel> channels = new HashMap<>();
    
    public static Lock activityLock = new ReentrantLock();
    
    public static long tickDelay = 2500; //2.5 seconds
    public static boolean running = false;
    public static long tick = 0;
    public static long time_out_time = 1000*60*10; //10 minutes
    
    
    public static ActivityLog log = new ActivityLog();

    public static void release() {
        activityLock.unlock();
    }
    
    private ActivityLog(){
        running = true;
        this.start();
    }
    
    public void run(){
        running = true;
        while(running){
            tick();
            tick++;
            
            try{
                Thread.sleep(tickDelay);
            } catch (Exception E){}
        }
    }
    
    public static void tick(){
        if(activityLock.tryLock()){
            for(int i = activeUsers.size() - 1; i >= 0; i--){
                UserData UD = activeUsers.get(i);
                activityCheck(UD);
            }
            activityLock.unlock();
        }
    }
    
    public static void activityCheck(UserData UD){
        long time = UD.lastMessage.getData();
        
        if(System.currentTimeMillis() - time > time_out_time){
            activeUsers.remove(UD);
        }
    }
    
    public static void update(UserData UD){
        long ID = UD.lastChannel.getData();
        long time = UD.lastMessage.getData();
        
        if(!activeUsers.contains(UD)){
            activeUsers.add(UD);
        }
        
        IChannel chan = Launcher.client.getChannelByID(ID);
        
        channels.put(UD, chan);
        
        GateChannel gate = GateChannel.get(chan);
        
        try{
            gate.currentArea.doBusiness(gate,UD);
            
            activateWeapons(true, UD,gate);
        } catch (Exception E){E.printStackTrace();}
    }
    
    public static void activateWeapons(boolean active, UserData UD, GateChannel gate){
        CraftNode CNA = CraftNode.getNode(UD.wepA.getData());
        CraftNode CNB = CraftNode.getNode(UD.wepB.getData());
        CraftNode CNC = CraftNode.getNode(UD.wepC.getData());
        
        //System.out.println(CNA);
        
        if(CNA.effect.getBoolean("Active")==active){
            EffectParser.parse(UD, gate, CNA.effect);
        }
        if(CNB.effect.getBoolean("Active")==active){
            EffectParser.parse(UD, gate, CNB.effect);
        }
        if(CNC.effect.getBoolean("Active")==active){
            EffectParser.parse(UD, gate, CNC.effect);
        }
    }
    
}
