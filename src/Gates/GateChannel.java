/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gates;

import User.UserData;
import Bot.Launcher;
import static Loot.SuperRandom.oRan;
import Commands.CommandParser;
import Commands.Implementation.Buy;
import Commands.Implementation.Charge;
import Commands.Implementation.Deposit;
import Commands.Implementation.NextArea;
import Commands.Implementation.Recolor;
import Commands.Implementation.WorldInfo;
import Farm.Area;
import Farm.CustomArea;
import Farm.Monster;
import static Farm.Monster.FAMILY_BEAST;
import static Farm.Monster.FAMILY_CONSTRUCT;
import static Farm.Monster.FAMILY_FIEND;
import static Farm.Monster.FAMILY_GREMLIN;
import static Farm.Monster.FAMILY_MINERAL;
import static Farm.Monster.FAMILY_SLIME;
import static Farm.Monster.FAMILY_TREASURE;
import static Farm.Monster.FAMILY_UNDEAD;
import Item.Structure.CraftPath;
import Item.Structure.Material;
import Item.Structure.Mineral;
import static Item.Structure.Mineral.ALLOY_WEIGHT;
import User.Field;
import com.vdurmont.emoji.EmojiManager;
import java.awt.Color;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;
import sx.blah.discord.handle.impl.obj.ReactionEmoji;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IReaction;
import sx.blah.discord.handle.obj.IUser;

/**
 *
 * @author andrewwhitmer
 */
public class GateChannel extends Thread{
    public IChannel chan;
    public CommandParser CP;
    
    public static final long TICK_DELAY = 30000; //30 seconds is the base time
    public boolean running = false;
    public long tick = 0;
    
    public Field<HashMap<String,Integer>> minerals;
    public Field<String> lastArea;
    public Area currentArea;
    
    private static HashMap<IChannel,GateChannel> worldMap = new HashMap<>();
    
    public GateChannel(IChannel set){
        this.chan = set;
        
        minerals = new Field<>("Gate",this.chan.getLongID(),new HashMap<String,Integer>());
        lastArea = new Field<>("Gate",this.chan.getLongID(),"lastArea","Mineral Quarry");
        currentArea = Area.getArea(lastArea.getData());
        
        try {
            Thread.sleep(100);
        } catch (InterruptedException ex) {}
        
        
        this.CP = new CommandParser();
        CP.addCommand(new WorldInfo());
        CP.addCommand(new Deposit());
        CP.addCommand(new Charge());
        CP.addCommand(new NextArea());
        CP.addCommand(new Buy());
        
        worldMap.put(this.chan, this);
        
        try{
            Thread.sleep(1337);
        } catch (Exception E){}
        
        this.start();
    }
    
    public void run(){
        running = true;
        while(running){
            
            this.tick();
            tick++;
            
            try{
                Thread.sleep(TICK_DELAY);
            } catch (Exception E){}
        }
    }
    
    public void tick(){
        while(!ActivityLog.activityLock.tryLock()){} 
        
        
        
        for(UserData UD : ActivityLog.activeUsers){
            if(ActivityLog.channels.get(UD).equals(this.chan)){
                //rewardTick(UD);
                
                ActivityLog.activateWeapons(false, UD, this);
            }
        }
        
        //CREATE NEW GATE IF ITS TIME
        if(tick >= (currentArea.gateTime / TICK_DELAY)){
            dealMinerals();
            //this.chan.changeName(currentArea.name.toLowerCase().replace(" ", "-"));
            tick = 0;
        }
        
        ActivityLog.release();
    }
    
    public void dealMinerals(){
        
        
        String [] mineralOrder = Mineral.sortMinerals(minerals.getData());
        
        int total = 0;
        for(String S : mineralOrder){
            if(S.contains("Alloy")){
                total += minerals.getData().get(S) * ALLOY_WEIGHT;
            } else {
                total+=minerals.getData().get(S);
            }
        }
        
        //Check if the top 3 are an alloy
        int alloy = 0;
        
        for(int i = 0; i < 3; ++i){
            if(mineralOrder.length > i){
                if(mineralOrder[i].contains("Alloy")){
                    alloy += 1;
                }
            } else {
                alloy++;
            }
        }
        
        if(total < ALLOY_WEIGHT) {
            if(!(currentArea.name.equals("Mineral Quarry")||currentArea.name.equals("Clockwork Terminal"))){
                currentArea = Area.getArea("Clockwork Terminal");
                this.lastArea.writeData(currentArea.name);
                //new GateMessage("GATE FORMATION COMPLETE!","Weclome to beautiful "+currentArea.name+"!").send(chan);
                return;
            }
            currentArea = Area.getArea("Mineral Quarry");
            this.lastArea.writeData(currentArea.name);
            //new GateMessage("GATE FORMATION COMPLETE!","Weclome to beautiful "+currentArea.name+"!").send(chan);
            return;
        }
        
        
        currentArea = Area.getArea(total,mineralOrder);
        
        if(currentArea == null){
            Area A = new CustomArea(minerals.getData());
            currentArea = A;
            this.lastArea.writeData(currentArea.name);
            new GateMessage("GATE FORMATION COMPLETE!","Weclome to beautiful "+currentArea.name+"!").send(chan);
            minerals.writeData(new HashMap<>());
            return;
        }
        
        this.lastArea.writeData(currentArea.name);
        new GateMessage("GATE FORMATION COMPLETE!","Weclome to beautiful "+currentArea.name+"!").send(chan);

        minerals.getData().clear();
        minerals.write();

    }
    
    public void receive(IMessage message){
        String content = message.getContent();
        IUser author = message.getAuthor();
        
        UserData UD = UserData.getUD(author.getLongID());
        
        this.CP.parseCommand(content, author.getLongID());
    }
    
    
    public static GateChannel get(IChannel channel){
        if(worldMap.containsKey(channel)){
            return worldMap.get(channel);
        }
        return null;
    }
    
    public static void parseMessage(IMessage message){
        IChannel from = message.getChannel();
        GateChannel sendTo = get(from);
        
        if(sendTo == null){
            return;
        } else {
            sendTo.receive(message);
        }
    }
    
    public GateMessage sendMessage(String title, String message){
        GateMessage mess = new GateMessage(title,message);
        mess.C = getColor();
        
        return mess;
    }
    
    public GateMessage gateInfo(){
        String gateData = currentArea.info(this);
        
        int time = (int)Math.ceil((currentArea.gateTime - this.tick*TICK_DELAY) / (60*1000));
        if(time < 1){
            time = 1;
        }
        gateData+="Time remaining till next gate: "+time+" minutes";
        
        String mineraltext = "";
        
        HashMap<String, Integer> minmap = minerals.getData();
        if(minmap.size() == 0){
            mineraltext = "Gate is empty...";
        } else {
            for(String key : minmap.keySet()){
                Mineral M = Mineral.getMineral(key);

                mineraltext+=M+": "+minmap.get(key)+"\n";
            }
        }
        
        String message = "Use the deposit command to deposit!\n";
        message+="E.G.\n";
        message+="```\n";
        message+="deposit blue 12\n";
        message+="```";
        
        GateMessage ret = sendMessage(this.currentArea.name,gateData+"\n\n**Minerals**\n\n"+mineraltext+"\n"+message);
        
        return ret;
    }
    
    public Color getColor(){
        
        return this.currentArea.getColor();
    }
    
    public static void loadAll(){
        Monster.instantiate();
        Area.initialize();        
        Material.loadMaterials();
        CraftPath.loadCraftingPaths();
        Mineral.generateMinerals();
        
        instantiateGates(Launcher.client.getGuilds().get(0));
    }
    
    public static void instantiateGates(IGuild G){
        for(IChannel chan : G.getChannels()){
            if(!worldMap.containsKey(chan)){
                new GateChannel(chan);
            }
        }
    }
    
    public void addMineral(String name, int amt){
        if(minerals.getData().containsKey(name)){
            int total = minerals.getData().get(name) + amt;
            minerals.getData().put(name, total);
            minerals.write();
        } else {
            minerals.getData().put(name, amt);
            minerals.write();
        }
    }
    
    public void addReaction(IReaction react) throws Exception{
        
    }

    public void removeReaction(IReaction reaction) {
        
    }
}
