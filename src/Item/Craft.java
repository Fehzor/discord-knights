/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Item;

import Display.AlloyView;
import Item.Structure.CraftNode;
import User.UserData;

/**
 *
 * @author FF6EB
 */
public class Craft {
    public static String create (UserData UD, String upgrade){
        CraftNode item = CraftNode.getNode(upgrade);
        if(item == null || item.pre.equals("None") || (item.recipe && !UD.recipes.getData().contains(upgrade))){
            ((AlloyView)UD.invObj.views.get("alloy")).craft(upgrade);
            return "CRAFTING FAILURE: Item name not found. Check for typos? ";
        }
        
        int amt = item.materialCost(item.stars);
        
        String mat1 = item.mat1;
        String mat2 = item.mat2;
        
        if(item.pre.equals("Craft") || UD.check(item.pre)){
            if(UD.checkMaterials(mat1, amt) && UD.checkMaterials(mat2, amt)){
                UD.takeMaterials(mat1, amt);
                UD.takeMaterials(mat2, amt);
                UD.take(item.pre);
                
                UD.give(item.name);
            } else {
                return "CRAFTING FAILURE: Not enough materials! \nNeed: "+amt+" "+mat1+"/"+mat2;
            }
        } else {
            return "CRAFTING FAILURE: Item required- "+item.pre;
        }
        
        return "CRAFTING SUCCESS!";
    }
}
