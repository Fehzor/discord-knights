/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Item.Parser;

import Farm.Monster;
import Gates.GateChannel;
import User.UserData;
import org.json.JSONObject;

/**
 *
 * @author FF6EB
 */
public class MineralDamageEffect extends ItemEffect{
    public MineralDamageEffect(){
        super("Mineral Damage");
    }
    
    public String effect(UserData UD, GateChannel chan, JSONObject json){
        
        int dmg = json.getInt("Amount");
        
        int aoe = 1;
        if(json.has("AOE")){
            aoe = json.getInt("AOE");
        }
        
        chan.currentArea.takeDamage(UD, chan, dmg,true, aoe);
        
        return "true";
    }
    
    public String description(JSONObject json){
        if(json.has("AOE")){
            int aoe = json.getInt("AOE");
            return "Deals an extra "+json.getInt("Amount")+" damage... to up to "+aoe+" minerals!";
        }
        return "Deals an extra "+json.getInt("Amount")+" damage... to minerals!";
    }
}
