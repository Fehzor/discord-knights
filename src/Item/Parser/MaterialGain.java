/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Item.Parser;

import Gates.GateChannel;
import User.UserData;
import org.json.JSONObject;

/**
 *
 * @author FF6EB
 */
public class MaterialGain extends ItemEffect{
    public MaterialGain(){
        super("Material Gain");
    }
    
    public String effect(UserData UD, GateChannel chan, JSONObject json){
        int amt = json.getInt("Amount");
        String mat = json.getString("Material");
        
        
        
        UD.giveMaterials(mat, amt);
        
        return "true";
    }
    
    public String description(JSONObject json){
        return "Grants the user "+json.getLong("Amount")+" "+json.getString("Material")+" material(s)";
    }
}
