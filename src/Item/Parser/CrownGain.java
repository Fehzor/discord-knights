/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Item.Parser;

import Gates.GateChannel;
import User.UserData;
import org.json.JSONObject;

/**
 *
 * @author FF6EB
 */
public class CrownGain extends ItemEffect{
    public CrownGain(){
        super("Crown Gain");
    }
    
    public String effect(UserData UD, GateChannel chan, JSONObject json){
        UD.crowns.append(json.getLong("Amount"));
        
        return "true";
    }
    
    public String description(JSONObject json){
        return "Grants the user "+json.getLong("Amount")+" crowns.";
    }
}
