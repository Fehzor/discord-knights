/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Item.Parser;

import Farm.Monster;
import Gates.GateChannel;
import User.UserData;
import org.json.JSONObject;

/**
 *
 * @author FF6EB
 */
public class DamageEffect extends ItemEffect{
    public DamageEffect(){
        super("Damage");
    }
    
    public String effect(UserData UD, GateChannel chan, JSONObject json){
        
        int dmg = json.getInt("Amount");
        int aoe = 1;
        if(json.has("AOE")){
            aoe = json.getInt("AOE");
        }
        
        chan.currentArea.takeDamage(UD, chan, dmg,false, aoe);
        
        return "true";
    }
    
    public String description(JSONObject json){
        if(json.has("AOE")){
            int aoe = json.getInt("AOE");
            return "Deals an extra "+json.getInt("Amount")+" damage to up to "+aoe+" enemies.";
        }
        return "Deals an extra "+json.getInt("Amount")+" damage.";
    }
}
