/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Item.Parser;

import Farm.Monster;
import Gates.GateChannel;
import User.UserData;
import org.json.JSONObject;

/**
 *
 * @author FF6EB
 */
public class GainCharge extends ItemEffect{
    public GainCharge(){
        super("Charge");
    }
    
    public String effect(UserData UD, GateChannel chan, JSONObject json){
        
        int dmg = json.getInt("Amount");
        
        UD.charges.append(dmg);
        
        
        return "true";
    }
    
    public String description(JSONObject json){
        return "Gain an extra "+json.getInt("Amount")+" charges!";
    }
}
