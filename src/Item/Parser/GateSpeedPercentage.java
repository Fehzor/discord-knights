/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Item.Parser;

import static Loot.SuperRandom.oRan;
import Gates.GateChannel;
import User.UserData;
import org.json.JSONObject;

/**
 *
 * @author FF6EB
 */
public class GateSpeedPercentage extends ItemEffect{
    boolean inTick = false;
    
    public GateSpeedPercentage(){
        super("Gate Speed Percent");
    }
    
    public String effect(UserData UD, GateChannel chan, JSONObject json){
        if(inTick){
            return "true";
        }
        inTick = true;
        int num = json.getInt("Percent");
        
        if(oRan.nextInt(100) < num){
            chan.tick();
        }
        
        inTick = false;
        return "true";
    }
    
    public String description(JSONObject json){
        return "Forces the gate to tick an extra time... "+json.getInt("Percent")+"% of the time it works every time.";
    }
}
