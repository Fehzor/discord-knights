/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Item.Parser;

import Farm.Monster;
import Gates.GateChannel;
import User.UserData;
import org.json.JSONObject;

/**
 *
 * @author FF6EB
 */
public class CatalyzerEffect extends ItemEffect{
    public CatalyzerEffect(){
        super("Catalyzer");
    }
    
    public String effect(UserData UD, GateChannel chan, JSONObject json){
        int dmg = json.getInt("Amount");
        
        appendData("Catalyzer",dmg);
        
        
        return "true";
    }
    
    public String description(JSONObject json){
        return "Stacks an extra "+json.getInt("Amount")+" damage on per message. Detonate to deal damage!";
    }
}
