/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Item.Parser;

import static Loot.SuperRandom.oRan;
import Gates.GateChannel;
import User.UserData;
import org.json.JSONObject;

/**
 *
 * @author FF6EB
 */
public class MineralGain extends ItemEffect{
    public MineralGain(){
        super("Mineral Gain");
    }
    
    public String effect(UserData UD, GateChannel chan, JSONObject json){
        String color = json.getString("Color");
        int amt = json.getInt("Amount");
        
        if(color.equals("Random")){
            int rand = oRan.nextInt(5);
        
        switch(rand){
            case 0:
                UD.giveMinerals("Red",amt);
                break;
            case 1:
                UD.giveMinerals("Green",amt);
                break;
            case 2:
                UD.giveMinerals("Blue",amt);
                break;
            case 3:
                UD.giveMinerals("Yellow",amt);
                break;
            case 4:
                UD.giveMinerals("Purple",amt);
                break;
            default:
                return "false";
        }
        } else {
            UD.giveMinerals(color, amt);
        }
        
        return "true";
    }
    
    public String description(JSONObject json){
        if(json.getBoolean("Active")){
            return "Grants the user "+json.getInt("Amount")+" "+json.getString("Color")+" minerals per message.";
        } else {
            return "Grants the user "+json.getInt("Amount")*((int)(60*1000/GateChannel.TICK_DELAY))+" "+json.getString("Color")+" every minute.";
        }
    }
}
