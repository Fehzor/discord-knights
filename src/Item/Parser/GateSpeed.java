/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Item.Parser;

import Gates.GateChannel;
import User.UserData;
import org.json.JSONObject;

/**
 *
 * @author FF6EB
 */
public class GateSpeed extends ItemEffect{
    boolean inTick = false;
    
    public GateSpeed(){
        super("Gate Speed");
    }
    
    public String effect(UserData UD, GateChannel chan, JSONObject json){
        if(inTick){
            return "true";
        }
        inTick = true;
        int num = json.getInt("Amount");
        
        for(int i = 0; i < num; ++i){
            chan.tick();
        }
        inTick = false;
        return "true";
    }
    
    public String description(JSONObject json){
        return "Forces the gate to tick "+json.getInt("Amount")+" time(s).";
    }
}
