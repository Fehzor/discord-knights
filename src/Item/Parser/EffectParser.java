/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Item.Parser;

import Gates.GateChannel;
import User.UserData;
import org.json.JSONObject;
import sx.blah.discord.handle.obj.IChannel;

/**
 *
 * @author FF6EB
 */
public class EffectParser {
    
    public static String parse(UserData UD, GateChannel chan, JSONObject json){
        try{
            ItemEffect.parse(UD,chan,json);
        } catch (Exception E){
            System.err.println("FAILURE TO PARSE: "+json.toString());
            E.printStackTrace();
        }
        
        return "false";
    }
}
