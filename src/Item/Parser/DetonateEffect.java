/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Item.Parser;

import Farm.Monster;
import Gates.GateChannel;
import User.UserData;
import org.json.JSONObject;

/**
 *
 * @author FF6EB
 */
public class DetonateEffect extends ItemEffect{
    public DetonateEffect(){
        super("Detonate");
    }
    
    public String effect(UserData UD, GateChannel chan, JSONObject json){
        
        int dmg = json.getInt("Amount") + getData("Catalyzer");
        putData("Catalyzer",0);
        
        chan.currentArea.takeDamage(UD, chan, dmg,false,3);
        
        
        return "true";
    }
    
    public String description(JSONObject json){
        return "Detonates catalyzer damage. Also deals "+json.getInt("Amount")+" damage to up to 3 enemies.";
    }
}
