/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Item.Parser;

import Gates.GateChannel;
import User.UserData;
import org.json.JSONObject;

/**
 *
 * @author FF6EB
 */
public class And extends ItemEffect{
    boolean inTick = false;
    
    public And(){
        super("And");
    }
    
    public String effect(UserData UD, GateChannel chan, JSONObject json){
        JSONObject effectA = json.getJSONObject("Effect A");
        JSONObject effectB = json.getJSONObject("Effect B");
        
        EffectParser.parse(UD, chan, effectA);
        EffectParser.parse(UD, chan, effectB);
        
        return "true";
    }
    
    public String description(JSONObject json){
        String A = ItemEffect.describe(json.getJSONObject("Effect A"));
        String B = ItemEffect.describe(json.getJSONObject("Effect B"));
        return "\n"+A+"\nAND\n"+B;
    }
}
