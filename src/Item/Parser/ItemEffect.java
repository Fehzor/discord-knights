/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Item.Parser;

import Gates.GateChannel;
import User.UserData;
import java.util.HashMap;
import org.json.JSONObject;

/**
 *
 * @author FF6EB
 */
public class ItemEffect {
    private static HashMap<String, ItemEffect> ItemFX = new HashMap<>();
    
    private static HashMap<String, Integer> data = new HashMap<>();
    
    private static ItemEffect whatever = new ItemEffect();
    private ItemEffect(){
        new DamageEffect();
        new MineralDamageEffect();
        
        new GainCharge();
        
        new CatalyzerEffect();
        new DetonateEffect();
        
        new CrownGain();
        new MineralGain();
        new GateSpeed();
        new GateSpeedPercentage();
        
        new And();
    }
    
    public ItemEffect(String name){
        ItemFX.put(name, this);
    }
    
    public String effect(UserData UD, GateChannel chan, JSONObject json){
        return "false";
    }
    
    public String description(JSONObject json){
        return "No effect.";
    }
    
    public static String parse(UserData UD, GateChannel chan, JSONObject json){
        return ItemFX.get(json.getString("Command")).effect(UD,chan,json);
    }
    
    public static String describe(JSONObject effect){
        return ItemFX.get(effect.getString("Command")).description(effect);
    }
    
    public static void putData(String key, int val){
        data.put(key, val);
    }
    
    public static void appendData(String key, int val){
        if(data.containsKey(key)){
            data.put(key, val + getData(key));
        } else {
            putData(key,val);
        }
    }
    
    public static int getData(String key){
        if(data.containsKey(key)){
            return data.get(key);
        } else {
            return 0;
        }
    }
    
}
