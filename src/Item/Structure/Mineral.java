/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Item.Structure;

import com.vdurmont.emoji.EmojiManager;
import java.awt.Color;
import java.util.HashMap;

/**
 *
 * @author FF6EB
 */
public class Mineral {
    public static final int ALLOY_WEIGHT = 10;
    
    public int ID;
    public String name;
    public String emoji;
    public Color color;
    
    private static HashMap<String,Mineral> mins = new HashMap<>();
    
    public Mineral(int ID, String name, Color color, String emoji){
        this.ID = ID;
        this.name = name;
        this.color = color;
        this.emoji = emoji;
        
        mins.put(ID+"",this);
        mins.put(name,this);
    }
    
    public static Mineral getMineral(String name){
        if(!mins.containsKey(name)){
            System.err.println("UNKNOWN MINERAL GIVEN ::: "+name);
            //throw new NullPointerException();
        }
        return mins.get(name);
    }
    
    public static void generateMinerals(){
        new Mineral(0,"Red",Color.RED,"<:red:563544758161768458>");
        new Mineral(1,"Green",Color.GREEN,"<:green:563544757721366540>");
        new Mineral(2,"Blue",Color.BLUE,"<:blue:563544757842739212>");
        new Mineral(3,"Yellow",Color.YELLOW,"<:yellow:563544758157443092>");
        new Mineral(4,"Purple",Color.MAGENTA,"<:purple:563544758123757598>");
        
        new Mineral(5,"Beast Alloy",Color.ORANGE,"<:beast:563530540636962819>");
        new Mineral(6,"Slime Alloy",Color.PINK,"<:slime:563530713375309845>");
        new Mineral(7,"Gremlin Alloy",Color.DARK_GRAY,"<:gremlin:563530637986758706>");
        new Mineral(8,"Construct Alloy",Color.LIGHT_GRAY,"<:construct:563530577349836812>");
        new Mineral(9,"Undead Alloy",Color.BLACK,"<:undead:563530739858276382>");
        new Mineral(10,"Fiend Alloy",Color.MAGENTA,"<:fiend:563530606424621056>");
        new Mineral(11,"Fire Alloy",Color.RED,"<:fire:563530798926528531>");
        new Mineral(12,"Freeze Alloy",Color.CYAN,"<:freeze:563530823350091786>");
        new Mineral(13,"Shock Alloy",Color.RED,"<:shock:563530868375683073>");
        new Mineral(14,"Poison Alloy",Color.RED,"<:poison:563530848247349249>");
        
        new Mineral(14,"Arena Alloy",Color.GREEN,"<:arena:564799100302917642>");
    }
    
    public String toString(){
        try{
            String moji = EmojiManager.getForAlias(emoji).getUnicode();
            return moji+name;
        } catch (Exception Exe){
            return emoji+" "+name;   
        }
    }
    
    public static String [] sortMinerals(HashMap<String, Integer> minerals){
        String [] sorted = new String[minerals.keySet().size()];
        
        int index = 0;
        String val = "";
        
        //Alloys are weighted heavier.
        for(String key : minerals.keySet()){
            if(key.contains("Alloy")){
                minerals.put(key, minerals.get(key) * ALLOY_WEIGHT);
            }
        }
        
        for(String key : minerals.keySet()){
            addTo(sorted, key, minerals);
        }
        System.out.println("MINERALS SORTED-");
        for(String S : sorted){
            System.out.println(S+" "+minerals.get(S));
        }
        
        for(String key : minerals.keySet()){
            if(key.contains("Alloy")){
                minerals.put(key, minerals.get(key) / ALLOY_WEIGHT);
            }
        }
        
        return sorted;
    }
    
    private static void addTo(String [] to, String add, HashMap<String,Integer> minerals){
        int index = 0;
        String temp = "";
        while(index < minerals.keySet().size()){
            if(!minerals.containsKey(to[index])){
                to[index] = add;
                return;
            } else if(minerals.get(add) > minerals.get(to[index])) {
                temp = add;
                add = to[index];
                to[index] = temp;
            }
            index++;
        }
    }
}
