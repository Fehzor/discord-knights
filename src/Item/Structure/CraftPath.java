/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Item.Structure;

import User.UserData;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;
import org.json.JSONObject;

/**
 *
 * @author FF6EB
 */
public class CraftPath {
    private HashMap<String, String> prereqs = new HashMap<>();
    
    private static ArrayList<CraftPath> paths = new ArrayList<>();
    
    public CraftPath(JSONObject json){
        Iterator<?> keys = json.keys();
        while( keys.hasNext() ) {
            String key = (String)keys.next();
            JSONObject val = json.getJSONObject(key);
            CraftNode add = new CraftNode(key,val);
            
            prereqs.put(key,add.pre);
        }
        
        paths.add(this);
    }
    
    public static void loadCraftingPaths(){
        JSONObject json;
        File F = new File("resources/paths.json");
        try{
            Scanner oScan = new Scanner(F);
            String get = "";
            while(oScan.hasNextLine()){
                get+=oScan.nextLine()+"\n";
            }
            json = new JSONObject(get);
            Iterator<?> keys = json.keys();
            while( keys.hasNext() ) {
                JSONObject jso = json.getJSONObject((String)keys.next());
                
                CraftPath make = new CraftPath(jso);
            }
        } catch (Exception EXE){EXE.printStackTrace();}
    }
    
    public static void setupCraftables(ArrayList<String> ret, UserData UD){
        for(CraftPath CP : paths){
            for(String key : CP.prereqs.keySet()){
                String pre = CP.prereqs.get(key);
                if(pre.equals("Craft") || UD.check(pre)){
                    if(CraftNode.getNode(key).recipe){
                        if(UD.recipes.getData().contains(key)){
                            ret.add(key);
                        }
                    } else {
                        ret.add(key);
                    }
                }
            }
        }
    }
}
