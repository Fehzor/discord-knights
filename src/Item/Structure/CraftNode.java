/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Item.Structure;

import Gates.GateChannel;
import Item.Parser.ItemEffect;
import static Loot.SuperRandom.oRan;
import com.vdurmont.emoji.EmojiManager;
import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONObject;

/**
 *
 * @author FF6EB
 */
public class CraftNode {
    public String name, desc, pre, mat1, mat2;
    public int stars;
    public JSONObject effect;
    public JSONObject charge;
    public boolean recipe = false;
    
    public static final int TYPE_NORMAL = 0;
    public static final int TYPE_ELEMENTAL = 1;
    public static final int TYPE_SHADOW = 2;
    public static final int TYPE_PIERCING = 3;
    
    public int damageType;
    
    private static HashMap<String, CraftNode> Items = new HashMap<>();
    
    public static ArrayList<String> commonRecipes = loadRecipes();
    public static ArrayList<String> loadRecipes(){
        ArrayList<String> ret = new ArrayList<>();
        
        ret.add("Cutter");
        ret.add("Graviton Charge");
        ret.add("Troika");
        ret.add("Piercing Blaster");
        ret.add("Shadow Blaster");
        ret.add("Elemental Blaster");
        ret.add("Brandish");
        
        return ret;
    }
    
    public static ArrayList<String> alloyRecipes = loadAlloyRecipes();
    public static ArrayList<String> loadAlloyRecipes(){
        ArrayList<String> ret = new ArrayList<>();
        
        ret.add("Fiend Alloy");
        ret.add("Undead Alloy");
        ret.add("Gremlin Alloy");
        ret.add("Fire Alloy");
        ret.add("Shock Alloy");
        ret.add("Poison Alloy");
        ret.add("Freeze Alloy");
        
        
        return ret;
    }
    
    public CraftNode(String name, JSONObject json){
        this.name = name;
        this.desc = json.getString("Desc");
        this.pre = json.getString("Pre");
        
        this.stars = json.getInt("Stars");
        this.mat1 = json.getString("Mat1");
        this.mat2 = json.getString("Mat2");
        
        Items.put(this.name,this);
        
        this.effect = json.getJSONObject("Effect");
        
        this.damageType = json.getInt("Damage");
        if(json.has("Recipe")){
            this.recipe = json.getBoolean("Recipe");
        }
        if(json.has("Charge")){
            this.charge = json.getJSONObject("Charge");
        }
        if(this.pre.equals("Craft")){
            this.recipe = true;
        }
        
    }
    
    
    public static CraftNode getNode(String name){
        return Items.get(name);
    }
    
    public static String getRandom(){
        int i = oRan.nextInt(Items.keySet().size());
        for(String name : Items.keySet()){
            i = i - 1;
            if(i <= 0){
                return name;
            }
        }
        return "Proto Saber";
    }
    
    public String toString(){
        
        int cost = materialCost(this.stars);
        
        String ret = "**"+this.name+": **"+desc + "\n";
        if(effect.getBoolean("Active")){
            ret+="**ON MESSAGE- **"+ItemEffect.describe(effect)+"\n";
        } else {
            ret+="**EVERY "+((int)(GateChannel.TICK_DELAY / 1000))+" SECONDS- **"+ItemEffect.describe(effect)+"\n";
        }
        
        if(charge != null){
            ret+="**ON CHARGE- **"+ItemEffect.describe(charge)+"\n";
        }
        if(cost == -1){
            return ret;
        }
        ret+="Crafting Cost: "+cost+"x "+mat1+" and "+cost+"x "+mat2+"\n";
        
        return ret;
    }
    
    public int materialCost(int stars){
        switch(stars){
            case 1:
                return 1;
            case 2:
                return 5;
            case 3:
                return 55;
            case 4:
                return 555;
            case 5:
                return 5555;
            default:
                return -1;
        }
    }
}
