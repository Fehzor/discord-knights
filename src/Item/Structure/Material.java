/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Item.Structure;

import Gates.GateChannel;
import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;
import org.json.JSONObject;

/**
 *
 * @author FF6EB
 */
public class Material {
    public String name;
    public String min1 = "None";
    public String min2 = "None";
    public String anti = "None";
    
    private static HashMap<String,Material> mats = new HashMap<>();
    
    public Material(String name, JSONObject json){
        this.name = name;
        if(json.has("Min1")){
            this.min1 = json.getString("Min1");
        }
        if(json.has("Min2")){
            this.min2 = json.getString("Min2");
        }
        this.anti = json.getString("Anti");
        
        mats.put(name,this);
    }
    
    public static Material getMaterial(String name){
        return mats.get(name);
    }
    
    public static Material getMaterial(String one, String two, String three){
        for(String S : mats.keySet()){
            Material M = mats.get(S);
            if(M.min1.equals(one) && M.min2.equals(two)){
                if(M.anti.equals(three)){
                    return getAnti(M.name);
                }
                return M;
            }
            if(M.min2.equals(one) && M.min1.equals(two)){
                if(M.anti.equals(three)){
                    return getAnti(M.name);
                }
                return M;
            }
        }
        return null;
    }
    
    private static Material getAnti(String anti){
        for(String S : mats.keySet()){
            Material M = mats.get(S);
            if(M.anti.equals(anti)){
                return M;
            }
        }
        return null;
    }
    
    public static void loadMaterials(){
        JSONObject json;
        File F = new File("resources/materials.json");
        try{
            Scanner oScan = new Scanner(F);
            String get = "";
            while(oScan.hasNextLine()){
                get+=oScan.nextLine()+"\n";
            }
            json = new JSONObject(get);
            Iterator<?> keys = json.keys();
            while( keys.hasNext() ) {
                String key = (String)keys.next();
                JSONObject mat = json.getJSONObject(key);
                
                Material make = new Material(key,mat);
            }
        } catch (Exception EXE){EXE.printStackTrace();}
    }
}
