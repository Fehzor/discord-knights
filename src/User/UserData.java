package User;

import User.Field;
import Bot.Launcher;
import static Loot.SuperRandom.oRan;
import Display.InventoryDisplay;
import java.awt.Color;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import org.json.JSONObject;
import sx.blah.discord.handle.obj.IRole;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.handle.obj.Permissions;
import sx.blah.discord.util.RoleBuilder;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author FF6EB4
 */
public class UserData {
    public static Field<ArrayList<String>> IDList = new Field<>("USERDATA","IDLIST",new ArrayList<>());
    private static HashMap<String,UserData> UserList = new HashMap<>();
    
    public static UserData getUD(IUser user){
        if(!UserList.containsKey(user.getStringID())){
            if(!IDList.getData().contains(user.getStringID())){
                IDList.getData().add(user.getStringID());
            }
            UserList.put(user.getStringID(),new UserData(user));
        }
        
        IDList.writeData(IDList.getData());
        
        //if(UserList.get(user.getID()).name.equals("Clint Eastwood's Character")){
        //    UserList.get(user.getID()).name = user.getName();
        //}
        
        return UserList.get(user.getStringID());
    }
    
    public static UserData getUD(long ID){
        return getUD(Launcher.client.getUserByID(ID));
        
    }
    
    
    public String name = "Nameless Hero Of Legend";
    public String ID = "00002";
    public InventoryDisplay invObj;
    
    public Field<Integer> lols;
    public Field<HashMap<String,Integer>> emoji;
    public Field<Integer> blocks;

    public Field<Long> role;
    
    public Field<Long> lastMessage;
    public Field<Long> lastChannel;
    
    
    public Field<String> wepA;
    public Field<String> wepB;
    public Field<String> wepC;
    
    public Field<Integer> charges;
    
    public Field<Long> crowns;
    public Field<HashMap<String, Integer>> minerals;
    public Field<HashMap<String, Integer>> materials;
    public Field<HashMap<String,Integer>> inventory; 
    public Field<ArrayList<String>> recipes;
    
    public UserData(IUser user){
        
        instantiateFields(user);
        
        invObj = new InventoryDisplay(this);
    }
    
    

    
    private void instantiateFields(IUser user){
        
        this.name = user.getName();
        this.ID = user.getStringID();
        
        lols = new Field<>(this.ID,"lols",0);
        emoji = new Field<>(this.ID,"emoji",new HashMap<>());
        blocks = new Field<>(this.ID,"blocks",0);
        
        role = new Field<>(this.ID,"role",this.getRoleIfApplicable(ID).getLongID());
        
        inventory = new Field<>(this.ID,"inventory",new HashMap<>());
        wepA = new Field<>(this.ID,"wepA","Proto Sword");
        wepB = new Field<>(this.ID,"wepB","Proto Gun");
        wepC = new Field<>(this.ID,"wepC","Proto Bomb");
        
        this.crowns = new Field<>(this.ID,"crowns",0L);
        this.minerals = new Field<>(this.ID,"minerals",new HashMap<>());
        this.materials = new Field<>(this.ID,"materials",new HashMap<>());
        
        this.lastMessage = new Field<>(this.ID,"lastMessage",0L);
        this.lastChannel = new Field<>(this.ID,"lastChannel",0L);
        
        this.charges = new Field<>(this.ID,"charges",10);
        
        ArrayList<String> gnuRecipe = new ArrayList<>();
        gnuRecipe.add("Calibur");
        gnuRecipe.add("Blaster");
        gnuRecipe.add("Blast Bomb");
        
        this.recipes = new Field<>(this.ID,"recipes",gnuRecipe);
    }
    
    private IRole getRoleIfApplicable(String ID){
        List<IRole> roles = Launcher.client.getRoles();
        
        for(IRole R : roles){
            if(R.getName().equals(""+ID)){
                return R;
            }
        }
        
        //Otherwise, create a role for the new friend!
        
        //GETS THE FIRST GUILD POSSIBLE FOR THIS
        RoleBuilder RB = new RoleBuilder(Launcher.client.getGuilds().get(0));
        
        RB.withName(ID);
        RB.withColor(new Color(oRan.nextInt(255),oRan.nextInt(255),oRan.nextInt(255)));
        
        IRole role = RB.build();
        return role;
    }
    
    public boolean check(String item){
        return this.inventory.getData().containsKey(item) &&
               this.inventory.getData().get(item) >= 0;
    }
    
    public void give(String item){
        if(this.inventory.getData().containsKey(item)){
            this.inventory.getData().put(item, this.inventory.getData().get(item)+1);
        } else {
            this.inventory.getData().put(item,1);
        }
        
        this.inventory.write();
        
        invObj.update(this);
        
    }
    
    public boolean take(String item){
        if(this.inventory.getData().containsKey(item)){
            this.inventory.getData().put(item, this.inventory.getData().get(item)-1);
            
            if(this.inventory.getData().get(item) <= 0){
                this.inventory.getData().remove(item);
            }
            
            this.inventory.write();
            
            invObj.update(this);
            
            return true;
        } else {
            return false;
        }
    }
    
    public void giveMinerals(String min, int num){
        if(this.minerals.getData().containsKey(min)){
            this.minerals.getData().put(min, this.minerals.getData().get(min)+num);
        } else {
            this.minerals.getData().put(min,num);
        }
        
        this.minerals.write();
    }
    
    public boolean takeMinerals(String min, int num){
        if(this.minerals.getData().containsKey(min) && this.minerals.getData().get(min)>=num){
            this.minerals.getData().put(min, this.minerals.getData().get(min)-num);
            
            if(this.minerals.getData().get(min) <= 0){
                this.minerals.getData().remove(min);
            }
            
            this.minerals.write();

            return true;
        } else {
            return false;
        }
    }
    
    public boolean checkMaterials(String mat, int num){
        return this.materials.getData().containsKey(mat) &&
               this.materials.getData().get(mat) >= num;
    }
    
    public void giveMaterials(String mat, int num){
        if(this.materials.getData().containsKey(mat)){
            this.materials.getData().put(mat, this.materials.getData().get(mat)+num);
        } else {
            this.materials.getData().put(mat,num);
        }
        
        this.materials.write();
    }
    
    public boolean takeMaterials(String mat, int num){
        if(this.materials.getData().containsKey(mat) && this.materials.getData().get(mat)>=num){
            this.materials.getData().put(mat, this.materials.getData().get(mat)-num);
            
            if(this.materials.getData().get(mat) <= 0){
                this.materials.getData().remove(mat);
            }
            
            this.materials.write();

            return true;
        } else {
            return false;
        }
    }
    
    public void addRecipe(String recipe){
        if(this.recipes.getData().contains(recipe)){
            return;
        }
        
        this.recipes.getData().add(recipe);
        this.recipes.write();
    }
}
