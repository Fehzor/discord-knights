package User;

/*
 * Copyright (C) 2017 FF6EB4
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author FF6EB4
 */
public class Field<Type> implements Serializable {
    private String signature;
    private Type data;
    
    private static ArrayList<Field> fields = new ArrayList<>();
    
    public Field(String name, Type def){
        this.signature = name;
        Field copy = load(signature);
        if(copy != null){
            try{
                this.data = (Type)copy.data;
            } catch (Exception E){
                this.data = def;
                System.out.println("Bad data loading "+name+"!  "+E);
            }
        } else {
            this.data = def;
        }
        
        fields.add(this);
        this.write();
    }
    
    public Field(String name, Object name2, Type def){
        this(name+"_"+name2,def);
    }
    
    public Field(String name, Object name2, Object name3, Type def){
        this(name+"_"+name2+"_"+name3,def);
    }
    
    public static Field load(String name){
        try{
            File F = new File("data/"+name);
            FileInputStream FIS = new FileInputStream(F);
            ObjectInputStream OIS = new ObjectInputStream(FIS);
            Object o = OIS.readObject();
            System.out.println("Loading..."+name);
            FIS.close();
            return (Field)o;
            
        } catch (Exception E){
            System.out.println("Failed to find "+name);
            return null;
        }
    }
    
    public void write(){
        try{
            File file = new File("data/"+signature);
            File parent = file.getParentFile();
            if (!parent.exists() && !parent.mkdirs()) { //create directory if it's missing
                System.out.println("Error: couldn't create data directory for file data/" + signature + "!");
                return;
            }
            FileOutputStream FOS = new FileOutputStream(file);
            ObjectOutputStream OOS = new ObjectOutputStream(FOS);
            
            OOS.writeObject(this);
            
            OOS.close();
        } catch (Exception E){
            System.out.println("Err saving field "+signature+"!  "+E);
        }
    }
    
    public Type getData(){
        return data;
    }
    
    public void append(Type add){
        if(this.getData() instanceof ArrayList){
            ArrayList dat = (ArrayList) this.getData();
            dat.add(add);
            this.write();
        }
        
        if(this.getData() instanceof String){
            String ret = (String)this.getData();
            ret += (String) add;
            this.writeData((Type)ret);
        }
        
        if(this.getData() instanceof Integer){
            int ret = (Integer)this.getData();
            ret += (Integer)add;
            this.writeData((Type)(Integer)ret);
        }
        
        if(this.getData() instanceof Double){
            double ret = (Double)this.getData();
            ret+= (Double)add;
            this.writeData((Type)(Double)ret);
        }
        
        if(this.getData() instanceof Long){
            long ret = (Long)this.getData();
            ret+= (Long)add;
            this.writeData((Type)(Long)(ret));
        }
        
        
    }
    
    public void writeData(Type dat){
        data = dat;
        this.write();
    }
}
