/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Farm;

import Gates.ActivityLog;
import Gates.GateChannel;
import Gates.GateMessage;
import User.UserData;

/**
 *
 * @author FF6EB
 */
class MonsterGen {
    public Area owner;
    
    public static final int RED_BOX_CHANCE = 7777;
    public static final int GREEN_BOX_CHANCE = 77;
    
    public MonsterGen(Area own){
        this.owner = own;
    }
    
    public void setup(){}
    
    public void register(String monster, int appear){
    
    }
    
    public void takeDamage(UserData UD, GateChannel chan, int damage, boolean mineral, int aoe){
        
    }
    
    public void giveDrops(GateChannel GC){
        
    }
    
    public String info(GateChannel GC){
        return "MonsterGen!";
    }
    
    public void doBusiness(GateChannel GC, UserData UD){
        
    }
}
