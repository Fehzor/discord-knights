/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Farm;

import Gates.GateChannel;
import Loot.Reward;
import static Loot.SuperRandom.oRan;
import User.UserData;
import java.util.HashMap;

/**
 *
 * @author FF6EB
 */
public class Mineral extends Monster{
    public static final int SHARD_CHANCE = 7777;
    
    public HashMap<GateChannel, Integer> size = new HashMap<>();
    public String color;
    public Mineral(String name){
        super(name+" Mineral", FAMILY_MINERAL, 1);
        
        color = name;
    }
    
    
    public int getMaxHealth(GateChannel GC){
        if(!size.containsKey(GC)){
            reset(GC);
        }
        return size.get(GC) * 10;
    }
    
    public void reset(GateChannel GC){
        size.put(GC,oRan.nextInt(5)+2);
        while(size.get(GC) == 5){
            size.put(GC,oRan.nextInt(5)+2);
        }
        
        this.reward.put(GC,generateReward(GC));
    }
    
    public Reward generateReward(GateChannel GC){
        Reward ret = new Reward();
        
        ret.minerals.put(color, size.get(GC));
        
        if(oRan.nextInt(SHARD_CHANCE) == 0){
            if(this.color.equals("Red")){
                ret.recipe = "Shard Bomb";
            }else if(this.color.equals("Blue")){
                ret.gear = "Ancient Catalyzer";
            }else if(this.color.equals("Yellow")){
                ret.recipe = "Splinter Bomb";
            }else if(this.color.equals("Green")){
                ret.recipe = "Crystal Bomb";
            }else if(this.color.equals("Purple")){
                ret.recipe = "Dark Matter Bomb";
            } else {
                ret.gear = "Ancient Catalyzer";
            }
        }
        
        return ret;
    }
}
