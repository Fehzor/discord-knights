/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Farm;

import Bot.Launcher;
import static Farm.Monster.FAMILY_MINERAL;
import Gates.ActivityLog;
import Gates.GateChannel;
import Gates.GateMessage;
import Item.Structure.CraftNode;
import static Loot.SuperRandom.oRan;
import User.UserData;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author FF6EB
 */
class MonsterGenArena extends MonsterGen{
    
    private ArrayList<String> monsterPool = new ArrayList<>();
    
    public final int MAX_ENEMIES = 5;
    
    public final int DROP_CHANCE = 3;
    
    
    
    int maxKills;
    public String [] boss = new String []{"None"};
    
    private int currentID = 0;
    
    // GC -> List of IDs
    private HashMap<GateChannel, ArrayList<Integer>> opponents = new HashMap<>();
    
    //ID, health
    private HashMap<Integer, Integer> health = new HashMap<>();
    //ID, name of monster
    private HashMap<Integer, String> IDList = new HashMap<>();
    
    public MonsterGenArena(Area own){
        super(own);
        setup();
    }
    
    public void register(String monster, int appear){
        for(int i = 0; i < appear; ++i){
            monsterPool.add(monster);
        }
    }
    
    public void setup(){}
    
    public void takeDamage(UserData UD, GateChannel chan, int damage, boolean mineral, int aoe){
        for(int i = 0; i < aoe; ++i){
            takeDamage(UD,chan,damage,mineral);
        }
    }
    
    private void takeDamage(UserData UD, GateChannel chan, int damage, boolean mineral){
        if(!opponents.containsKey(chan)){
            populate(chan);
        }
        ArrayList<Integer> monsterList = opponents.get(chan);
        int ID = monsterList.get(oRan.nextInt(monsterList.size()));
        
        Monster what = Monster.getMonster(this.IDList.get(ID));
        
        if(mineral && what.family != FAMILY_MINERAL){
            damage = 1;
        }
        if(!mineral && what.family == FAMILY_MINERAL){
            damage = 1;
        }
        
        health.put(ID, health.get(ID) - damage);
        if(health.get(ID) <= 0){
            owner.kills.append(1);
            if(owner.kills.getData() % 150 == 0){
                GateMessage GM = new GateMessage(owner.kills.getData()+" enemies killed in this area!","The invasion is going swimmingly!");
                GM.send(Launcher.client.getChannelByID(UD.lastChannel.getData()));
            }
            
            giveDrops(chan, ID);
            
            removeMonster(chan, ID);
            
        }
    }
    
    private void populate(GateChannel chan){
        if(!opponents.containsKey(chan)){
            opponents.put(chan, new ArrayList<>());
        }
        
        String monster = "";
        if(oRan.nextInt(RED_BOX_CHANCE) == 0){
            monster = "Red Prize Box";
        } else if(oRan.nextInt(GREEN_BOX_CHANCE) == 0){
            monster = "Green Prize Box";
        }  else if(owner.kills.getData() >= this.maxKills){
            monster = "Boss";
        } else {
            monster = "Other";
        }
        
        for(int i = 0; i < MAX_ENEMIES; ++i){
            if(monster.equals("Other")){
                addMonster(chan,monsterPool.get(oRan.nextInt(monsterPool.size())));
            } else if(monster.equals("Boss")){
                for(String S : boss){
                    addMonster(chan, S);
                }
                owner.kills.writeData(0);
            } else {
                addMonster(chan, monster);
            }
            
        }
    }
    
    public void giveDrops(GateChannel GC, int ID){
        String names = "";
        String finnally = "";
        for(UserData data : ActivityLog.activeUsers){
            if(oRan.nextInt(DROP_CHANCE) == 0 && ActivityLog.channels.get(data) == GC.chan){
                names+=data.name+", ";
                finnally = Monster.getMonster(IDList.get(ID)).giveDrops(GC,data);
            }
        }
        if(names.equals("")){
            return;
        }
        names += ""+finnally;
        new GateMessage("LOOT!",names).send(GC.chan);
    }
    
    public String info(GateChannel GC){
        if(!opponents.containsKey(GC)){
            populate(GC);
        }
        
        String ret = "";
        for(int ID : opponents.get(GC)){
            Monster mon = Monster.getMonster(IDList.get(ID));
            ret+=mon.info(false);
            ret+="HP remaining: "+health.get(ID)+"\n";
        }
        return ret+"\n";
    }
    
    public void doBusiness(GateChannel chan, UserData UD){
        if(!opponents.containsKey(chan)){
            populate(chan);
        }
        ArrayList<Integer> monsterList = opponents.get(chan);
        int ID = monsterList.get(oRan.nextInt(monsterList.size()));
        
        Monster what = Monster.getMonster(this.IDList.get(ID));
        
        int rand = oRan.nextInt(3);
        int damageType = 0;
        int stars = 0;
        if(rand == 0){
            damageType = CraftNode.getNode(UD.wepA.getData()).damageType;
            stars = CraftNode.getNode(UD.wepA.getData()).stars;
        }
        if(rand == 1){
            damageType = CraftNode.getNode(UD.wepB.getData()).damageType;
            stars = CraftNode.getNode(UD.wepB.getData()).stars;
        }
        if(rand == 2){
            damageType = CraftNode.getNode(UD.wepC.getData()).damageType;
            stars = CraftNode.getNode(UD.wepC.getData()).stars;
        }
        
        int damage = what.getHit(damageType,stars);
        
        this.takeDamage(UD, chan, damage, false);
    }
    
    public void addMonster(GateChannel chan, String monster){
        int ID = currentID;
        currentID++;
        
        opponents.get(chan).add(ID);
        int HP = 1;
        
        System.out.println("LOADING... "+monster);
        HP+=Monster.getMonster(monster).getMaxHealth(chan) / 2;
        HP+=oRan.nextInt((1+Monster.getMonster(monster).getMaxHealth(chan) / 4));
        HP+=owner.kills.getData();
        
        health.put(ID, HP);
        IDList.put(ID, monster);
    }
    
    public void removeMonster(GateChannel GC, int ID){
        opponents.get(GC).remove(opponents.get(GC).indexOf(ID));
        health.remove(ID);
        IDList.remove(ID);
        
        if(opponents.get(GC).size()==0){
            populate(GC);
        }
    }
}
