/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Farm;

import static Farm.Monster.FAMILY_BEAST;
import static Farm.Monster.FAMILY_CONSTRUCT;
import static Farm.Monster.FAMILY_FIEND;
import static Farm.Monster.FAMILY_FIRE;
import static Farm.Monster.FAMILY_FREEZE;
import static Farm.Monster.FAMILY_GREMLIN;
import static Farm.Monster.FAMILY_POISON;
import static Farm.Monster.FAMILY_SHOCK;
import static Farm.Monster.FAMILY_SLIME;
import static Farm.Monster.FAMILY_UNDEAD;
import static Farm.Monster.UNCOMMON_DROP_RATE;
import Loot.Reward;
import static Loot.SuperRandom.oRan;
import java.awt.Color;

/**
 *
 * @author FF6EB
 */
public class BaseArea extends Area{
    
    public int fam;
    public int depth;
    
    public BaseArea(String name, int fam, int depth){
            super(name);
            this.fam = fam;
            this.gateTime = 60 * 1000 * 4 * depth;
            
            MG = new MonsterGenProgression(this){
                public void setup(){
                    for(int i = 0; i < depth; ++i){
                    this.register(monsterTiers(fam,0),5 + (50 * i));
                    this.register(monsterTiers(fam,1),15 + (50 * i));
                    this.register(monsterTiers(fam,2),25 + (50 * i));
                    this.register(monsterTiers(fam,3),35 + (50 * i));
                    this.register(monsterTiers(fam,4),45 + (50 * i));


                    this.maxKills = depth * 55;
                    this.boss = monsterTiers(fam,5);
                    }
                }
            };
            
            this.color = getColor(fam);
            
            this.minimumMineralAmount=(int)Math.pow(7, depth);
            this.mineralList=getMinerals(fam);
        }
        
        public void setPrize(Reward set){
            
            switch(fam){
                case FAMILY_BEAST:
                    set.recipe = "Beast Alloy";
                    break;
                case FAMILY_SLIME:
                    set.recipe = "Slime Alloy";
                    break;
                case FAMILY_GREMLIN:
                    set.recipe = "Gremlin Alloy";
                    break;
                case FAMILY_CONSTRUCT:
                    set.recipe = "Construct Alloy";
                    break;
                case FAMILY_FIEND:
                    set.recipe = "Fiend Alloy";
                    break;
                case FAMILY_UNDEAD:
                    set.recipe = "Undead Alloy";
                    break;
                case FAMILY_FIRE:
                    set.recipe = "Fire Alloy";
                    break;
                case FAMILY_SHOCK:
                    set.recipe = "Shock Alloy";
                    break;
                case FAMILY_POISON:
                    set.recipe = "Poison Alloy";
                    break;
                case FAMILY_FREEZE:
                    set.recipe = "Freeze Alloy";
                    break;
            }
            
            if(oRan.nextInt(UNCOMMON_DROP_RATE) == 0){
                set.recipe = "Arena Alloy";
            }
        }
        
        public static Color getColor(int fam){
            switch(fam){
                case FAMILY_BEAST:
                    return Color.YELLOW;
                case FAMILY_SLIME:
                    return new Color(255,110,180);
                case FAMILY_GREMLIN:
                    return Color.ORANGE;
                case FAMILY_CONSTRUCT:
                    return Color.LIGHT_GRAY;
                case FAMILY_FIEND:
                    return Color.MAGENTA;
                case FAMILY_UNDEAD:
                    return Color.DARK_GRAY;
                case FAMILY_FIRE:
                    return Color.RED;
                case FAMILY_SHOCK:
                    return Color.CYAN;
                case FAMILY_POISON:
                    return Color.GREEN;
                case FAMILY_FREEZE:
                    return Color.BLUE;
            }
            return Color.WHITE;
        }
        
        public String[][] getMinerals(int family){
            String [][] ret = new String[6][3];
            String [] mins = new String[3];
            switch(family){
                case FAMILY_BEAST:
                    mins[0] = "Beast Alloy";
                    mins[1] = "Red";
                    mins[2] = "Green";
                    break;
                case FAMILY_SLIME:
                    mins[0] = "Slime Alloy";
                    mins[1] = "Yellow";
                    mins[2] = "Green";
                    break;
                case FAMILY_GREMLIN:
                    mins[0] = "Gremlin Alloy";
                    mins[1] = "Red";
                    mins[2] = "Blue";
                    break;
                case FAMILY_CONSTRUCT:
                    mins[0] = "Construct Alloy";
                    mins[1] = "Blue";
                    mins[2] = "Yellow";
                    break;
                case FAMILY_FIEND:
                    mins[0] = "Fiend Alloy";
                    mins[1] = "Red";
                    mins[2] = "Purple";
                    break;
                case FAMILY_UNDEAD:
                    mins[0] = "Undead Alloy";
                    mins[1] = "Purple";
                    mins[2] = "Yellow";
                    break;
                case FAMILY_FIRE:
                    mins[0] = "Fire Alloy";
                    mins[1] = "Red";
                    mins[2] = "Yellow";
                    break;
                case FAMILY_SHOCK:
                    mins[0] = "Shock Alloy";
                    mins[1] = "Purple";
                    mins[2] = "Blue";
                    break;
                case FAMILY_POISON:
                    mins[0] = "Poison Alloy";
                    mins[1] = "Purple";
                    mins[2] = "Green";
                    break;
                case FAMILY_FREEZE:
                    mins[0] = "Freeze Alloy";
                    mins[1] = "Blue";
                    mins[2] = "Green";
                    break;
            }
            
            AreaList.permute(ret, mins);
            
            return ret;
        }
        
        public static final int TIER_MINI = 0;
        public static final int TIER_SMALL = 1;
        public static final int TIER_BASIC = 2;
        public static final int TIER_ELITE = 3;
        public static final int TIER_TURRET = 4;
        public static final int TIER_GIANT = 5;
        
        public static String monsterTiers(int family, int tier){
            switch(family){
                case FAMILY_BEAST:
                    switch(tier){
                        case TIER_MINI:
                            return "Dustbun";
                        case TIER_SMALL:
                            return "Chromalisk";
                        case TIER_BASIC:
                            return "Wolver";
                        case TIER_ELITE:
                            return "ALpha Wolver";
                        case TIER_TURRET:
                            return "Howlitzer";
                        case TIER_GIANT:
                            return "Snarbolax";
                    }
                    break;
                case FAMILY_SLIME:
                    switch(tier){
                        case TIER_MINI:
                            return "Glop Drop";
                        case TIER_SMALL:
                            return "Lichen";
                        case TIER_BASIC:
                            return "Slime Cube";
                        case TIER_ELITE:
                            return "Mecha Knight";
                        case TIER_TURRET:
                            return "Polyp";
                        case TIER_GIANT:
                            return "Giant Lichen Colony";
                    }
                    break;
                case FAMILY_GREMLIN:
                    switch(tier){
                        case TIER_MINI:
                            return "Gremlin Civilian";
                        case TIER_SMALL:
                            return "Knocker";
                        case TIER_BASIC:
                            return "Gremlin Demo";
                        case TIER_ELITE:
                            return "Thwacker";
                        case TIER_TURRET:
                            return "Gun Puppy";
                        case TIER_GIANT:
                            return "Collector";
                    }
                    break;
                case FAMILY_CONSTRUCT:
                    switch(tier){
                        case TIER_MINI:
                            return "Scrap Metal";
                        case TIER_SMALL:
                            return "Scuttlebot";
                        case TIER_BASIC:
                            return "Retrode";
                        case TIER_ELITE:
                            return "Mecha Knight";
                        case TIER_TURRET:
                            return "Gun Puppy";
                        case TIER_GIANT:
                            return "Collector";
                    }
                    break;
                case FAMILY_FIEND:
                    switch(tier){
                        case TIER_MINI:
                            return "Tiny Devilite";
                        case TIER_SMALL:
                            return "Devilite";
                        case TIER_BASIC:
                            return "Gorgo";
                        case TIER_ELITE:
                            return "Greaver";
                        case TIER_TURRET:
                            return "Howlitzer";
                        case TIER_GIANT:
                            return "Trojan";
                    }
                    break;
                case FAMILY_UNDEAD:
                    switch(tier){
                        case TIER_MINI:
                            return "Grave Scarab";
                        case TIER_SMALL:
                            return "Spookat";
                        case TIER_BASIC:
                            return "Dust Zombie";
                        case TIER_ELITE:
                            return "Phantom";
                        case TIER_TURRET:
                            return "Howlitzer";
                        case TIER_GIANT:
                            return "Deadnaut";
                    }
                    break;
                case FAMILY_FIRE:
                    switch(tier){
                        case TIER_MINI:
                            return "Sun Scarab";
                        case TIER_SMALL:
                            return "Volcanic Whisp";
                        case TIER_BASIC:
                            return "Slag Walker";
                        case TIER_ELITE:
                            return "Gremlin Scorcher";
                        case TIER_TURRET:
                            return "Red Rover";
                        case TIER_GIANT:
                            return "Gremlin Incinerator";
                    }
                    break;
                case FAMILY_FREEZE:
                    switch(tier){
                        case TIER_MINI:
                            return "Snow Drop";
                        case TIER_SMALL:
                            return "Winter Whisp";
                        case TIER_BASIC:
                            return "Frostifur";
                        case TIER_ELITE:
                            return "Ice Cube";
                        case TIER_TURRET:
                            return "Polar Polyp";
                        case TIER_GIANT:
                            return "Blue Green Trojan";
                    }
                    break;
                case FAMILY_SHOCK:
                    switch(tier){
                        case TIER_MINI:
                            return "Power Drop";
                        case TIER_SMALL:
                            return "Storm Whisp";
                        case TIER_BASIC:
                            return "Volt Knight";
                        case TIER_ELITE:
                            return "Quicksilver";
                        case TIER_TURRET:
                            return "Silver Polyp";
                        case TIER_GIANT:
                            return "Blue Purple Trojan";
                    }
                    break;
                case FAMILY_POISON:
                    switch(tier){
                        case TIER_MINI:
                            return "Blech Bunny";
                        case TIER_SMALL:
                            return "Bog Whisp";
                        case TIER_BASIC:
                            return "Hurkat";
                        case TIER_ELITE:
                            return "Jade Greaver";
                        case TIER_TURRET:
                            return "Vile Howlitzer";
                        case TIER_GIANT:
                            return "Golden Bog Whisp";
                    }
                    break;
            }
            
            
            
            System.err.println("ERROR NONE RETURNED PARAMS="+family+" "+tier);
            return "None";
        }
}
