/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Farm;

import static Farm.Monster.FAMILY_BEAST;
import static Farm.Monster.FAMILY_SLIME;
import Item.Structure.CraftNode;
import static Item.Structure.Mineral.ALLOY_WEIGHT;
import Loot.Reward;
import static Loot.SuperRandom.oRan;
import User.Field;
import java.awt.Color;
import java.util.HashMap;

/**
 *
 * @author FF6EB
 */
public class CustomArea extends Area{
    
    public static Field<HashMap<String, HashMap<String,Integer>>> storage = new Field<>("CUSTOM_STORAGE",new HashMap<>());;
    
    public HashMap<String, Integer> base;
    public String [] sorted;
    public int total = 0;
    
    public CustomArea(String name){
        this(storage.getData().get(name));
    }
    
    public CustomArea(HashMap<String, Integer> minerals){
        super(createName(minerals));
        base = minerals;
        
        this.mineralList = new String[][]{{"None"}};
        
        storage.getData().put(name, minerals);
        storage.write();
        
        this.sorted = Item.Structure.Mineral.sortMinerals(minerals);
        
        MG = new MonsterGenProgression(this);
        
        for(String S : minerals.keySet()){
            if(S.contains("Alloy")){
                minerals.put(S,minerals.get(S)*ALLOY_WEIGHT);
            }
        }
        
        for(String S : minerals.keySet()){
            this.total = minerals.get(S);
        }
        
        int num = 0;
        if(sorted.length == 1){
            num = minerals.get(sorted[0]);
            for(int i = 0; i < num; i += oRan.nextInt(1+(num/4))){
                registerByTheme(sorted[0].split(" ")[0],i);
            }
            
            
        }
        if(sorted.length == 2){
            num = minerals.get(sorted[0]) + minerals.get(sorted[1]);
            for(int i = 0; i < num; i += oRan.nextInt(1+(num/7))){
                registerByTheme(sorted[oRan.nextInt(1+oRan.nextInt(2))].split(" ")[0],i);
            }
        }
        
        if(sorted.length > 2){
            num = minerals.get(sorted[0]) + minerals.get(sorted[1]) + minerals.get(sorted[2]);
            for(int i = 0; i < num; i += oRan.nextInt(1+(num/9))){
                registerByTheme(sorted[oRan.nextInt(1+oRan.nextInt(1+oRan.nextInt(3)))].split(" ")[0],i);
            }
        }
        
        this.minimumMineralAmount = num;
        ((MonsterGenProgression)MG).maxKills = num;
        ((MonsterGenProgression)MG).boss = Monster.getRandomMonster(sorted[0].split(" ")[0]).name;
        this.color = Color.LIGHT_GRAY;
    }
    
    private void registerByTheme(String theme, int num){
        Monster M = Monster.getRandomMonster(theme);
        String name = M.name;
        ((MonsterGenProgression)MG).register(name, num);
    }
    
    private static String createName(HashMap<String, Integer> minerals){
        String [] sorted = Item.Structure.Mineral.sortMinerals(minerals);
        
        if(sorted.length == 1){
            return "Pure "+sorted[0].split(" ")[0]+" Gate";
        }
        if(sorted.length == 2){
            String end = sorted[0].split(" ")[0];
            String pre = sorted[1].split(" ")[0];
            
            return getPrefix(pre)+" "+getNoun(end);
        }
        
        if(sorted.length > 2){
            String end = sorted[0].split(" ")[0];
            String pre = sorted[1].split(" ")[0];
            String prepre = sorted[2].split(" ")[0];
            
            return getOtherPrefix(prepre)+" "+getPrefix(pre)+" "+getNoun(end);
        }
        
        return "Wacky Filler World";
    }
    
    private static String getOtherPrefix(String base){
        switch (base){
            case "Beast":
                return "Growling";
            case "Slime":
                return "Slick";
            case "Gremlin":
                return "Deconstructed";
            case "Construct":
                return "Modern";
            case "Fiend":
                return "Mean";
            case "Undead":
                return "Stinky";
            case "Fire":
                return "Warm";
            case "Shock":
                return "Sparky";
            case "Poison":
                return "Foul";
            case "Freeze":
                return "Chilly";
            default:
                return base;
        }
    }
    
    private static String getPrefix(String base){
        switch (base){
            case "Beast":
                return "Snarly";
            case "Slime":
                return "Sluggish";
            case "Gremlin":
                return "Snickering";
            case "Construct":
                return "Beeping";
            case "Fiend":
                return "Devilish";
            case "Undead":
                return "Undying";
            case "Fire":
                return "Flaming Hot";
            case "Shock":
                return "Electric";
            case "Poison":
                return "Toxic";
            case "Freeze":
                return "Icy";
            default:
                return base;
        }
    }
    
    private static String getNoun(String base){
        switch (base){
            case "Beast":
                return "Den";
            case "Slime":
                return "Slimeway";
            case "Gremlin":
                return "Deconstruction Zone";
            case "Construct":
                return "Factory";
            case "Fiend":
                return "Lair";
            case "Undead":
                return "Morgue";
            case "Fire":
                return "Oven";
            case "Shock":
                return "Generator";
            case "Poison":
                return "Apothecary";
            case "Freeze":
                return "Refridgerator";
            default:
                return base+" Rainbow";
        }
    }
    
    
    public void setPrize(Reward set){
        set.crowns = 7777;
        
        int add = 3 + (int)Math.floor(total / 100);
        for(int i = 0; i < sorted.length && add > 0; ++i){
            for(int j = 0; j < base.get(sorted[i]) && add > 0;++j){
                switch(sorted[i]){
                    case "Beast":
                        set.materials.put("Beast", add);
                        break;
                    case "Slime":
                        set.materials.put("Slime", add);
                        break;
                    case "Gremlin":
                        set.materials.put("Gremlin", add);
                        break;
                    case "Construct":
                        set.materials.put("Construct", add);
                        break;
                    case "Fiend":
                        set.materials.put("Fiend", add);
                        break;
                    case "Undead":
                        set.materials.put("Undead", add);
                        break;
                    case "Fire":
                        set.materials.put("Fire", add);
                        break;
                    case "Shock":
                        set.materials.put("Shock", add);
                        break;
                    case "Freeze":
                        set.materials.put("Freeze", add);
                        break;
                    case "Poison":
                        set.materials.put("Poison", add);
                        break;
                }
                add = add - 1;
            }
        }
        
    }
}
