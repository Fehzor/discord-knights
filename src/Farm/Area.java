/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Farm;

import Farm.AreaList;
import Bot.Launcher;
import Farm.CustomArea;
import Farm.MonsterGen;
import static Loot.SuperRandom.oRan;
import Gates.GateChannel;
import Gates.GateMessage;
import Item.Structure.CraftNode;
import User.Field;
import User.UserData;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import static Farm.Monster.FAMILY_MINERAL;
import Gates.ActivityLog;
import Loot.Reward;
import java.awt.Color;

/**
 *
 * @author FF6EB
 */
public class Area {
    //Basic info
    public String name;
    public Color color = Color.GRAY;
    public int gateTime = 15 * 60 * 1000; //15 minutes
    
    public Field<Integer> kills;
    
    private static HashMap<String, Area> areaList = new HashMap<>();
    
    MonsterGen MG;
    
    public int minimumMineralAmount;
    public String[][]mineralList;
    
    Area(String name){
        this.name = name;
        this.kills = new Field(this.name,"kills",0);
        areaList.put(name, this);
        minimumMineralAmount = 100;
    }
    
    public static Area getArea(String name){
        if(areaList.containsKey(name)){
            return areaList.get(name);
        } else {
            return new CustomArea(name);
        }
    }
    
    //Based on a set of minerals, find a matching area.
    public static Area getArea(int amount, String[]mineralList){
        Area ret = null;
        if(mineralList == null || mineralList.length == 0 || mineralList[0].equals("None")){
            return ret;
        }
        
        int values = 3;
        int i = 0;
        int longest = 0;
        for(String key : areaList.keySet()){
            Area check = areaList.get(key);
            
            if(amount >= check.minimumMineralAmount){
            
                int j = 0;
                
                System.out.println(check.name);
                System.out.println(check.name+" "+check.mineralList.length);
                while(j < check.mineralList.length){
                    
                    
                    while(  check.mineralList[j].length > i &&
                            mineralList.length > i &&
                            check.mineralList[j][i].equals(mineralList[i])){
                        ++i;
                    }
                    if(i > longest)longest = i;
                    i=0;
                    ++j;
                }
                
                System.out.println(check.name+"="+longest);

                if(longest>values){
                    values = longest;
                    ret = check;
                } else if(longest == values){
                    if(ret.minimumMineralAmount < check.minimumMineralAmount){
                        ret = check;
                    }
                }
                longest = 0;
            }
        }
        return ret;
    }
    
    
    public void doBusiness(GateChannel gate, UserData UD){
        MG.doBusiness(gate, UD);
    }
    
    public void takeDamage(UserData UD, GateChannel chan, int damage, boolean mineral, int aoe){
        MG.takeDamage(UD, chan, damage, mineral, aoe);
    }
    
    public static void initialize(){
        new AreaList();
    }
    
    public Color getColor(){
        return this.color;
    }
    
    public String info(GateChannel GC){
        
        String ret = MG.info(GC);
                
        ret+="Current Kills In This Area: "+kills.getData()+"\n";
        ret+="\n";
        
        return ret;
    }
    
    public void setPrize(Reward set){
        set.crowns = 777;
        
        int what = oRan.nextInt(11);
        switch(what){
            case 0:
                set.materials.put("Beast", 10);
                break;
            case 1:
                set.materials.put("Slime", 10);
                break;
            case 2:
                set.materials.put("Gremlin", 10);
                break;
            case 3:
                set.materials.put("Construct", 10);
                break;
            case 4:
                set.materials.put("Fiend", 10);
                break;
            case 5:
                set.materials.put("Undead", 10);
                break;
            case 6:
                set.recipe = CraftNode.commonRecipes.get(oRan.nextInt(CraftNode.commonRecipes.size()));
                break;
            case 7:
                set.materials.put("Fire", 10);
                break;
            case 8:
                set.materials.put("Shock", 10);
                break;
            case 9:
                set.materials.put("Freeze", 10);
                break;
            case 10:
                set.materials.put("Poison", 10);
                break;
        }
    }
}
