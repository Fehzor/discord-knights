/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Farm;

import Gates.GateChannel;
import Item.Structure.CraftNode;
import static Loot.SuperRandom.oRan;
import User.Field;
import User.UserData;
import java.awt.Color;
import java.util.ArrayList;

/**
 *
 * @author FF6EB
 */
public class Terminal extends Area{
    
    public static Field<Long> lastSet = new Field<>("TERMINAL","LAST",System.currentTimeMillis());
    public static Field<ArrayList<String>> selling = new Field<>("TERMINAL","SELLING",getRecipes());
    
    public static final long RECIPE_RESET_TIME = 1000 * 60 * 60 * 3; // 3 hours why not
    
    public static final int MIN_RECIPES = 2;
    public static final int MAX_RECIPES = 4;
    
    public static final int RECIPE_COST = 25000;
    
    public Terminal(){
        super("Clockwork Terminal");
        gateTime = 1 * 90 * 1000; //1 minute + 30 seconds
        
        this.mineralList = new String[][]{
            {
                "None"
            }
        };
    }
    
    public void doBusiness(GateChannel gate, UserData UD){
        long time = lastSet.getData();
        if(System.currentTimeMillis() - time> RECIPE_RESET_TIME){
            selling.writeData(getRecipes());
            lastSet.writeData(System.currentTimeMillis());
        }
    }
    public void takeDamage(UserData UD, GateChannel chan, int damage, boolean mineral, int aoe){}
    
    public Color getColor(){
        return Color.ORANGE;
    }
    
    public static ArrayList<String> getRecipes(){
        ArrayList<String> ret = new ArrayList<>();
        
        while(ret.size() < MIN_RECIPES || (ret.size() < MAX_RECIPES && oRan.nextInt(MAX_RECIPES)<MIN_RECIPES)){
            String add = CraftNode.commonRecipes.get(oRan.nextInt(CraftNode.commonRecipes.size()));
            if(!ret.contains(add)){
                ret.add(add);
            }
        }
        
        //ret.add(CraftNode.alloyRecipes.get(oRan.nextInt(CraftNode.alloyRecipes.size())));
        
        return ret;
    }
    
    public String info(GateChannel GC){
        String ret = "**BASIL HAS THESE RECIPES IN STOCK**\n";
        
        ArrayList<String> recipes = selling.getData();
        
        for(String S : recipes){
            ret+=S+"\n";
        }
        
        ret+="\n";
        ret+="**Each costs "+RECIPE_COST+" crowns.**\n";
        ret+="\n";
        ret+="```\n";
        ret+="buy [recipe name]\n";
        ret+="```";
        ret+="\n";
        
        return ret;
    }
    
}
