/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Farm;

import Bot.Launcher;
import static Farm.Monster.FAMILY_MINERAL;
import Gates.ActivityLog;
import Gates.GateChannel;
import Gates.GateMessage;
import Item.Structure.CraftNode;
import static Loot.SuperRandom.oRan;
import User.Field;
import User.UserData;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author FF6EB
 */
public class MonsterGenProgression extends MonsterGen{
    
    private ArrayList<String> monsters = new ArrayList<>();
    private HashMap<String, Integer> appearance = new HashMap<>();
    
    int maxKills;
    public String boss = "None";
    
    private HashMap<GateChannel, String> opponent = new HashMap<>();
    private HashMap<GateChannel, Integer> health = new HashMap<>();
    
    public MonsterGenProgression(Area own){
        super(own);
        setup();
    }
    
    
    
    public void register(String monster, int appear){
        monsters.add(monster);
        appearance.put(monster, appear);
    }
    
    public String next( GateChannel GC){
        int killcount = owner.kills.getData();
        
        if(this.opponent.containsKey(GC) && this.opponent.get(GC).equals(boss)){
            owner.kills.writeData(0);
            return "Green Prize Box";
        } else if(oRan.nextInt(RED_BOX_CHANCE) == 0){
            return "Red Prize Box";
        } else if(oRan.nextInt(GREEN_BOX_CHANCE) == 0){
            return "Green Prize Box";
        }
        if(killcount >= maxKills){
            return boss;
        }
        
        int rand = oRan.nextInt(1+killcount);
        
        for(String mon : monsters){
            if(rand < appearance.get(mon)){
                Monster.getMonster(mon).reset(GC);
                return mon;
            }
        }
        
        
        return "Rock Jelly";
    }
    
    public void setNext(GateChannel GC){
        this.opponent.put(GC, next(GC));
        int hpMultiplier = (int)Math.ceil(Math.sqrt(owner.minimumMineralAmount));
        
        if(hpMultiplier < 1 || Monster.getMonster(this.opponent.get(GC)).family == FAMILY_MINERAL){
            hpMultiplier = 1;
        }
        this.health.put(GC,hpMultiplier * Monster.getMonster(this.opponent.get(GC)).getMaxHealth(GC));
    }
    
    public void doBusiness(GateChannel GC, UserData UD){
        if(!this.opponent.containsKey(GC)){
            setNext(GC);
        }
        
        int rand = oRan.nextInt(3);
        int damageType = 0;
        int stars = 0;
        if(rand == 0){
            damageType = CraftNode.getNode(UD.wepA.getData()).damageType;
            stars = CraftNode.getNode(UD.wepA.getData()).stars;
        }
        if(rand == 1){
            damageType = CraftNode.getNode(UD.wepB.getData()).damageType;
            stars = CraftNode.getNode(UD.wepB.getData()).stars;
        }
        if(rand == 2){
            damageType = CraftNode.getNode(UD.wepC.getData()).damageType;
            stars = CraftNode.getNode(UD.wepC.getData()).stars;
        }
        
        int damage = Monster.getMonster(this.opponent.get(GC)).getHit(damageType,stars);
        
        
        takeDamage(UD, GC, damage);
        
    }
    
    public void takeDamage(UserData UD, GateChannel chan, int damage, boolean mineral, int aoe){
        if(mineral){
            mineralHits(UD,chan,damage);
        } else {
            takeDamage(UD, chan, damage);
        }
    }
    
    private void mineralHits(UserData UD, GateChannel GC, int damage){
        if(!this.opponent.containsKey(GC)){
            setNext(GC);
        }
        
        if(Monster.getMonster(opponent.get(GC)).family != FAMILY_MINERAL){
            damage = 1;
        }
        
        this.health.put(GC, this.health.get(GC) - damage);
        
        if(this.health.get(GC) <= 0){
            owner.kills.append(1);
            if(owner.kills.getData() % 100 == 0){
                GateMessage GM = new GateMessage(owner.kills.getData()+" enemies killed in this area!","The invasion is going swimmingly!");
                GM.send(Launcher.client.getChannelByID(UD.lastChannel.getData()));
            }
            
            giveDrops(GC);
            
            
            this.opponent.put(GC, next(GC));
            this.health.put(GC,Monster.getMonster(this.opponent.get(GC)).getMaxHealth(GC));
        }
    }
    
    private void takeDamage(UserData UD, GateChannel GC, int damage){
        if(!this.opponent.containsKey(GC)){
            setNext(GC);
        }
        
        if(Monster.getMonster(opponent.get(GC)).family == FAMILY_MINERAL){
            damage = 1;
        }
        
        this.health.put(GC, this.health.get(GC) - damage);
        
        if(this.health.get(GC) <= 0){
            owner.kills.append(1);
            
            giveDrops(GC);
            
            
            setNext(GC);
        }
    }
    
    public void giveDrops(GateChannel GC){
        
        String names = "";
        String finnally = "";
        for(UserData data : ActivityLog.activeUsers){
            if(ActivityLog.channels.get(data) == GC.chan){
                names+=data.name+", ";
                finnally = Monster.getMonster(this.opponent.get(GC)).giveDrops(GC,data);
            }
        }
        
        names += ""+finnally;
        new GateMessage("LOOT!",names).send(GC.chan);
    }
    
    public String info(GateChannel GC){
        String mon = this.opponent.get(GC);
        String ret = "";
        
        ret+=Monster.getMonster(mon).info(true);
        ret+="Opponent's Health: "+health.get(GC)+" remaining\n";
        return ret;
    }
}
