/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Farm;

import static Farm.Monster.*;
import Farm.MonsterGenArena;
import Farm.MonsterGenProgression;
import Farm.Terminal;
import Gates.GateChannel;
import Loot.Reward;
import static Loot.SuperRandom.oRan;
import java.awt.Color;
import java.util.List;

/**
 *
 * @author FF6EB
 */
public class AreaList {
    private static int IMBADOKIGETIT = 0;
    
    public AreaList(){
        new Neutral();
        
        //Basic areas
        for(int i = 0; i < 10; ++i){
            new BaseArea("Aurora Isles D"+i,FAMILY_BEAST,i);
            new BaseArea("Lichenous Layer D"+i,FAMILY_SLIME,i);
            new BaseArea("Deconstruction Zone D"+i,FAMILY_GREMLIN,i);
            new BaseArea("Compact Area D"+i,FAMILY_CONSTRUCT,i);
            new BaseArea("Devilish Drudgery D"+i,FAMILY_FIEND,i);
            new BaseArea("Graveyard D"+i,FAMILY_UNDEAD,i);
            
            new BaseArea("Blast Furnace D"+i,FAMILY_FIRE,i);
            new BaseArea("Power Complex D"+i,FAMILY_SHOCK,i);
            new BaseArea("Wasteworks D"+i,FAMILY_POISON,i);
            new BaseArea("Cooling Chamber D"+i,FAMILY_FREEZE,i);
            
            new BaseArena("Beastly Brawl Arena D"+i,FAMILY_BEAST,i);
            new BaseArena("Slimy Showdown Arena D"+i,FAMILY_SLIME,i);
            new BaseArena("Wrench Warfare Arena D"+i,FAMILY_GREMLIN,i);
            new BaseArena("Iron Edge Arena Arena D"+i,FAMILY_CONSTRUCT,i);
            new BaseArena("Fiendish Fray Arena D"+i,FAMILY_FIEND,i);
            new BaseArena("Cadaverous Clash Arena D"+i,FAMILY_UNDEAD,i);
            
            new BaseArena("Flame Lash Arena D"+i,FAMILY_FIRE,i);
            new BaseArena("Thunder Fist Arena D"+i,FAMILY_SHOCK,i);
            new BaseArena("Venom Fang Arena D"+i,FAMILY_POISON,i);
            new BaseArena("Ice Maul Arena D"+i,FAMILY_FREEZE,i);
        }
        
        new Terminal();
    }
    
    private class Neutral extends Area{
        private Neutral(){
            super("Mineral Quarry");
            
            this.gateTime = 60 * 1000 * 5;
            minimumMineralAmount = 0;
            this.mineralList=new String[][]{{
                "None"
            }};
            
            this.MG = new MonsterGenProgression(this){
                public String next(GateChannel GC){
                    int rand = oRan.nextInt(25);
                    switch (rand){
                        case 0:
                        case 1:
                        case 2:
                        case 3:    
                            return "Red Mineral";
                        case 4:
                        case 5:
                        case 6:
                        case 7:
                            return "Green Mineral";
                        case 8:
                        case 9:
                        case 10:
                        case 11:
                            return "Blue Mineral";
                        case 12:
                        case 13:
                        case 14:
                        case 15:
                            return "Yellow Mineral";
                        case 16:
                        case 17:
                        case 18:
                        case 19:
                            return "Purple Mineral";
                        case 20:
                            return "Chromalisk";
                        case 21:
                            return "Retrode";
                        case 22:
                            return "Lumber";
                        case 23:
                            return "Rock Jelly";
                        case 24:
                            return "Lichen";
                    }
                    return "Rock Jelly";
                }
            };
        }
        
        
        
        public Color getColor(){
            return new Color(oRan.nextInt(255),oRan.nextInt(255),oRan.nextInt(255));
        }
    }
    
    
    public static void permute(String[][] result, String[] list){
        IMBADOKIGETIT = 0;
        permute(result, list, 0);
    }
    
    private static void sanspermute(String[][] result, String[] list){
        permute(result, list, 0);
    }
    
    public static void permute(String [][] list, String[] arr, int k){
        for(int i = k; i < arr.length; i++){
            swap(arr, i, k);
            permute(list,arr, k+1);
            swap(arr, k, i);
        }
        if (k == arr.length -1){
            for(int i = 0; i < arr.length; ++i){
                list[IMBADOKIGETIT][i] = arr[i];
            }
            IMBADOKIGETIT++;
        }
    } 
    
    private static void swap(String[]list, int a, int b){
        String temp = list[a];
        list[a] = list[b];
        list[b] = temp;
    }
    
}
