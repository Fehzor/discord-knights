/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Farm;

import Bot.Launcher;
import Gates.GateChannel;
import Gates.GateMessage;
import Item.Structure.CraftNode;
import Loot.Reward;
import static Loot.SuperRandom.oRan;
import User.UserData;
import java.util.ArrayList;
import java.util.HashMap;
import sx.blah.discord.handle.obj.IChannel;

/**
 *
 * @author FF6EB
 */
public class Monster {
    public static final int COMMON_DROP_RATE = 5;
    public static final int UNCOMMON_DROP_RATE = 10;
    public static final int RARE_DROP_RATE = 20;
    public static final int SUPER_RARE_DROP_RATE = 50;
    public static final int LEGENDARY_DROP_RATE = 100;
    public static final int MYTHICAL_DROP_RATE = 500;
    public static final int ABOVE_MYTHICAL_DROP_RATE = 1000;
    public static final int UNFAIR_DROP_RATE = 10000;
    
    public static final int BASE_HEALTH = 5;
    
    public static final int FAMILY_MINERAL = 0;
    public static final int FAMILY_BEAST = 1;
    public static final int FAMILY_SLIME = 2;
    public static final int FAMILY_GREMLIN = 3;
    public static final int FAMILY_CONSTRUCT = 4;
    public static final int FAMILY_FIEND = 5;
    public static final int FAMILY_UNDEAD = 6;
    public static final int FAMILY_TREASURE = 7;
    public static final int FAMILY_FIRE = 8;
    public static final int FAMILY_FREEZE = 9;
    public static final int FAMILY_SHOCK = 10;
    public static final int FAMILY_POISON = 11;

    public String name;
    public int family;
    private int hits;

    public static final int TYPE_NORMAL = 0;
    public static final int TYPE_ELEMENTAL = 1;
    public static final int TYPE_SHADOW = 2;
    public static final int TYPE_PIERCING = 3;

    public static final int EFFECTIVE_DAMAGE = 12;
    public static final int NORMAL_DAMAGE = 8;
    public static final int NEUTRAL_DAMAGE = 6;
    public static final int RESISTED_DAMAGE = 2;
    
    public HashMap<GateChannel, Reward> reward = new HashMap<>();
    
    private static HashMap<String, Monster> monsterList = new HashMap<>();
    private static HashMap<Integer, ArrayList<String>> byFamily = new HashMap<>();
    
    public Monster(String name, int family, int life){
        this.name = name;
        this.hits = life;
        this.family = family;
        monsterList.put(name, this);
        if(!byFamily.containsKey(family)){
            byFamily.put(family,new ArrayList<>());
        }
        byFamily.get(family).add(name);
        
    }
    
    public Monster(String name, int family, int otherFam, int life){
        this(name,family,life);
        if(!byFamily.containsKey(otherFam)){
            byFamily.put(otherFam,new ArrayList<>());
        }
        byFamily.get(otherFam).add(name);
    }
    
    public static Monster getMonster(String name){
        return monsterList.get(name);
    }
    
    public static Monster getRandomMonster(String theme){
        ArrayList fam = null;
        switch (theme.toLowerCase()){
            case "treasure":
                fam = byFamily.get(FAMILY_TREASURE);
                break;
            case "mineral":
                fam = byFamily.get(FAMILY_MINERAL);
                break;
            case "beast":
                fam = byFamily.get(FAMILY_BEAST);
                break;
            case "slime":
                fam = byFamily.get(FAMILY_SLIME);
                break;
            case "gremlin":
                fam = byFamily.get(FAMILY_GREMLIN);
                break;
            case "construct":
                fam = byFamily.get(FAMILY_BEAST);
                break;
            case "fiend":
                fam = byFamily.get(FAMILY_FIEND);
                break;
            case "undead":
                fam = byFamily.get(FAMILY_UNDEAD);
                break;
            case "fire":
                fam = byFamily.get(FAMILY_FIRE);
                break;
            case "shock":
                fam = byFamily.get(FAMILY_SHOCK);
                break;
            case "poison":
                fam = byFamily.get(FAMILY_POISON);
                break;
            case "freeze":
                fam = byFamily.get(FAMILY_FREEZE);
                break;
            case "red":
                return getMonster("Red Mineral");
            case "blue":
                return getMonster("Blue Mineral");
            case "green":
                return getMonster("Green Mineral");
            case "yellow":
                return getMonster("Yellow Mineral");
            case "purple":
                return getMonster("Purple Mineral");
                
        }
        
        if(fam == null){
            return null;
        }
        
        return getMonster((String) fam.get(oRan.nextInt(fam.size())));
    }
    
    public int getMaxHealth(GateChannel GC){
        int ret = (int)Math.ceil((double)hits * (double)0.8888);
        int variance = (int)Math.ceil((double)hits * (double)0.2222);
        ret += oRan.nextInt(variance);
        
        return ret;
    }
    
    public int getHit(int star, int type){
        int hit = -1;
        switch(family){
            case FAMILY_MINERAL:
                hit = 1;
                break;
            case FAMILY_BEAST:
                switch(type){
                    case TYPE_NORMAL:
                        hit = NORMAL_DAMAGE;
                        break;
                    case TYPE_ELEMENTAL:
                        hit = RESISTED_DAMAGE;
                        break;
                    case TYPE_SHADOW:
                        hit = NEUTRAL_DAMAGE;
                        break;
                    case TYPE_PIERCING:
                        hit = EFFECTIVE_DAMAGE;
                        break;
                }
                break;
            case FAMILY_SLIME:
                switch(type){
                    case TYPE_NORMAL:
                        hit = NORMAL_DAMAGE;
                        break;
                    case TYPE_ELEMENTAL:
                        hit = NEUTRAL_DAMAGE;
                        break;
                    case TYPE_SHADOW:
                        hit = EFFECTIVE_DAMAGE;
                        break;
                    case TYPE_PIERCING:
                        hit = RESISTED_DAMAGE;
                        break;
                }
                break;
            case FAMILY_GREMLIN:
                switch(type){
                    case TYPE_NORMAL:
                        hit = NORMAL_DAMAGE;
                        break;
                    case TYPE_ELEMENTAL:
                        hit = RESISTED_DAMAGE;
                        break;
                    case TYPE_SHADOW:
                        hit = EFFECTIVE_DAMAGE;
                        break;
                    case TYPE_PIERCING:
                        hit = NEUTRAL_DAMAGE;
                        break;
                }
                break;
            case FAMILY_CONSTRUCT:
                switch(type){
                    case TYPE_NORMAL:
                        hit = NORMAL_DAMAGE;
                        break;
                    case TYPE_ELEMENTAL:
                        hit = EFFECTIVE_DAMAGE;
                        break;
                    case TYPE_SHADOW:
                        hit = NEUTRAL_DAMAGE;
                        break;
                    case TYPE_PIERCING:
                        hit = RESISTED_DAMAGE;
                        break;
                }
                break;
            case FAMILY_FIEND:
                switch(type){
                    case TYPE_NORMAL:
                        hit = NORMAL_DAMAGE;
                        break;
                    case TYPE_ELEMENTAL:
                        hit = NEUTRAL_DAMAGE;
                        break;
                    case TYPE_SHADOW:
                        hit = RESISTED_DAMAGE;
                        break;
                    case TYPE_PIERCING:
                        hit = EFFECTIVE_DAMAGE;
                        break;
                }
                break;
            case FAMILY_UNDEAD:
                switch(type){
                    case TYPE_NORMAL:
                        hit = NORMAL_DAMAGE;
                        break;
                    case TYPE_ELEMENTAL:
                        hit = EFFECTIVE_DAMAGE;
                        break;
                    case TYPE_SHADOW:
                        hit = RESISTED_DAMAGE;
                        break;
                    case TYPE_PIERCING:
                        hit = NEUTRAL_DAMAGE;
                        break;
                }
                break;
        }
        
        int damageDealt = hit + star;
        if(hit == -1){
            damageDealt = 1;
        }
        //DAMAGE TABLE- (12, 8, 6, 2) + star level
        //2 STAR
        //weak = 14
        //norm = 10
        //neut = 8
        //resi = 4
        //3 STAR
        //weak = 15
        //norm = 11
        //neut = 9
        //resi = 5
        //4 STAR
        //weak = 16
        //norm = 12
        //neut = 10
        //resi = 6
        //5 STAR
        //weak = 17
        //norm = 13
        //neut = 11
        //resi = 7
        
        return damageDealt;
    }
    
    public String giveDrops(GateChannel GC, UserData UD){
        if(reward.get(GC) == null){
            reward.put(GC,generateReward(GC));
        }
        String msg = reward.get(GC).give(UD);
        IChannel chan = Launcher.client.getChannelByID(UD.lastChannel.getData());
        return msg;
    }
    
    public void reset(GateChannel GC){
        if(!this.reward.containsKey(GC)){
            this.reward.put(GC, generateReward(GC));
        }
    }
    
    public Reward generateReward(GateChannel GC){
        Reward ret = new Reward();
        
        if(family == FAMILY_BEAST){
            ret.materials.put("Beast", 1);
        }
        if(family == FAMILY_SLIME){
            ret.materials.put("Slime", 1);
        }
        if(family == FAMILY_GREMLIN){
            ret.materials.put("Gremlin", 1);
        }
        if(family == FAMILY_CONSTRUCT){
            ret.materials.put("Construct", 1);
        }
        if(family == FAMILY_FIEND){
            ret.materials.put("Fiend", 1);
        }
        if(family == FAMILY_UNDEAD){
            ret.materials.put("Undead", 1);
        }
        
        if(family != FAMILY_MINERAL){
            ret.crowns = (long)((this.hits * this.hits)/BASE_HEALTH);
        }
        
        if(family == FAMILY_TREASURE){
            GC.currentArea.setPrize(ret);
        }
        
        return ret;
    }
    
    public String info(boolean showFam){
        String gateData = "";
        String family = "None";
        if(this.family == FAMILY_BEAST){
            family = "Beast";
        }
        if(this.family == FAMILY_SLIME){
            family = "Slime";
        }
        if(this.family == FAMILY_GREMLIN){
            family = "Gremlin";
        }
        if(this.family == FAMILY_CONSTRUCT){
            family = "Construct";
        }
        if(this.family == FAMILY_FIEND){
            family = "Fiend";
        }
        if(this.family == FAMILY_UNDEAD){
            family = "Undead";
        }
        if(this.family == FAMILY_MINERAL){
            family = "Mineral";
        }
        if(this.family == FAMILY_TREASURE){
            family = "Treasure";
        }
        
        gateData+="**Opponent:** "+this.name+"\n";
        if(showFam){
            gateData+="Opponent's Family: "+family+"\n";
        }
        
        return gateData;
    }
    
    public static void instantiate(){
        int mineralHits = 1;
        
        String[] minerals = new String[]{
            "Red",
            "Green",
            "Blue",
            "Yellow",
            "Purple"
        };
        
        for(String S : minerals){
            new Mineral(S);
        }
        
        new Monster("Green Prize Box",FAMILY_TREASURE,1);
        new Monster("Red Prize Box",FAMILY_TREASURE,1){
            public Reward generateRewards(GateChannel GC){
                Reward ret = super.generateReward(GC);
                ret.gear = CraftNode.getRandom();
                ret.crowns = 777777;
                return ret;
            }
        };
        
        new Monster("Training Bot",FAMILY_MINERAL,2);
        
        new Monster("Dustbun",FAMILY_BEAST, BASE_HEALTH);
        new Monster("Blech Bunny",FAMILY_BEAST, BASE_HEALTH);
        new Monster("Wolver",FAMILY_BEAST, BASE_HEALTH*3);
        new Monster("Chromalisk",FAMILY_BEAST, BASE_HEALTH*5);
        new Monster("Alpha Wolver",FAMILY_BEAST, BASE_HEALTH*10);
        new Monster("Frostifur",FAMILY_BEAST, FAMILY_FREEZE, BASE_HEALTH*4);
        new Monster("Tundralisk",FAMILY_BEAST, FAMILY_FREEZE, BASE_HEALTH*6);
        
        new Monster("Tiny Devilite",FAMILY_FIEND, BASE_HEALTH/2);
        new Monster("Devilite",FAMILY_FIEND, BASE_HEALTH*1);
        new Monster("Gorgo",FAMILY_FIEND, BASE_HEALTH*3);
        new Monster("Yesman",FAMILY_FIEND, BASE_HEALTH*2);
        new Monster("Pit Boss",FAMILY_FIEND, BASE_HEALTH*10);
        new Monster("Overtimer",FAMILY_FIEND, BASE_HEALTH*6);
        new Monster("Greaver",FAMILY_FIEND, BASE_HEALTH*7);
        new Monster("Jade Greaver",FAMILY_FIEND, FAMILY_POISON,BASE_HEALTH*7);
        new Monster("Trojan",FAMILY_FIEND, BASE_HEALTH*20){
            public Reward generateReward(GateChannel chan){
                Reward ret = super.generateReward(chan);
                
                if(oRan.nextInt(RARE_DROP_RATE) == 0){
                    ret.recipe = "Troika";
                }
                
                return ret;
            }
        };
        new Monster("Blue Green Trojan",FAMILY_FIEND, BASE_HEALTH*20){
            public Reward generateReward(GateChannel chan){
                Reward ret = super.generateReward(chan);
                
                if(oRan.nextInt(RARE_DROP_RATE) == 0){
                    ret.recipe = "Jalovec";
                }
                
                return ret;
            }
        };
        new Monster("Blue Purple Trojan",FAMILY_FIEND, BASE_HEALTH*20){
            public Reward generateReward(GateChannel chan){
                Reward ret = super.generateReward(chan);
                
                if(oRan.nextInt(ABOVE_MYTHICAL_DROP_RATE) == 0){
                    ret.gear = "Gram";
                }
                
                return ret;
            }
        };
        
        new Monster("Glop Drop", FAMILY_SLIME,BASE_HEALTH);
        new Monster("Snow Drop", FAMILY_SLIME, FAMILY_FREEZE,BASE_HEALTH);
        new Monster("Power Drop", FAMILY_SLIME, FAMILY_SHOCK,BASE_HEALTH);
        new Monster("Polyp", FAMILY_SLIME,BASE_HEALTH);
        new Monster("Polar Polyp", FAMILY_SLIME, FAMILY_FREEZE,BASE_HEALTH);
        new Monster("Silver Polyp", FAMILY_SLIME, FAMILY_SHOCK,BASE_HEALTH){
            public Reward generateReward(GateChannel chan){
                Reward ret = super.generateReward(chan);
                
                if(oRan.nextInt(RARE_DROP_RATE) == 0){
                    ret.recipe = "Electron Charge";
                }
                
                return ret;
            }
        };
        new Monster("Slime Cube",FAMILY_MINERAL,FAMILY_SLIME,BASE_HEALTH*5);
        new Monster("Rock Jelly",FAMILY_SLIME,BASE_HEALTH*7);
        new Monster("Lichen",FAMILY_SLIME,BASE_HEALTH*3);
        new Monster("Lichen Colony",FAMILY_SLIME,BASE_HEALTH*8);
        new Monster("Giant Lichen Colony",FAMILY_SLIME,BASE_HEALTH*15);
        new Monster("Ice Cube",FAMILY_SLIME,FAMILY_FREEZE,BASE_HEALTH*8);
        new Monster("Quicksilver",FAMILY_SLIME, FAMILY_SHOCK,BASE_HEALTH*4){
            public Reward generateReward(GateChannel chan){
                Reward ret = super.generateReward(chan);
                
                if(oRan.nextInt(RARE_DROP_RATE) == 0){
                    ret.recipe = "Electron Charge";
                }
                
                return ret;
            }
        };
        
        new Monster("Gremlin Civilian",FAMILY_GREMLIN, BASE_HEALTH);
        new Monster("Gremlin Scorcher",FAMILY_GREMLIN,FAMILY_FIRE, BASE_HEALTH*4);
        new Monster("Gremlin Thwacker",FAMILY_GREMLIN, BASE_HEALTH*5);
        new Monster("Gremlin Mender",FAMILY_GREMLIN, BASE_HEALTH*7);
        new Monster("Gremlin Knocker",FAMILY_GREMLIN, BASE_HEALTH*1);
        new Monster("Gremlin Demo",FAMILY_GREMLIN, BASE_HEALTH*6);
        new Monster("Gremlin Stalker",FAMILY_GREMLIN, BASE_HEALTH*13);
        new Monster("Gremlin Incinerator",FAMILY_GREMLIN,FAMILY_FIRE,BASE_HEALTH*13);
        
        
        new Monster("Gremlin Gizmo",FAMILY_CONSTRUCT,FAMILY_GREMLIN,5);
        new Monster("Scrap Metal",FAMILY_CONSTRUCT,5);
        new Monster("Scuttlebot",FAMILY_CONSTRUCT,BASE_HEALTH*3);
        new Monster("Mecha Knight",FAMILY_CONSTRUCT,BASE_HEALTH*7);
        new Monster("Volt Knight",FAMILY_CONSTRUCT,BASE_HEALTH*7);
        new Monster("Retrode",FAMILY_CONSTRUCT,BASE_HEALTH*6);
        new Monster("Gun Puppy",FAMILY_CONSTRUCT,BASE_HEALTH*6);
        new Monster("Red Rover",FAMILY_CONSTRUCT,FAMILY_FIRE,BASE_HEALTH*4);
        new Monster("Lumber",FAMILY_CONSTRUCT,BASE_HEALTH*8);
        new Monster("Collector",FAMILY_CONSTRUCT,BASE_HEALTH*12);
        
        new Monster("Grave Scarab",FAMILY_UNDEAD,BASE_HEALTH);
        new Monster("Sun Scarab",FAMILY_UNDEAD,FAMILY_FIRE,BASE_HEALTH);
        new Monster("Dust Zombie",FAMILY_UNDEAD,BASE_HEALTH*5);
        new Monster("Slag Walker",FAMILY_UNDEAD,FAMILY_FIRE,BASE_HEALTH*6);
        new Monster("Phantom",FAMILY_UNDEAD,BASE_HEALTH*10){
            public Reward generateReward(GateChannel chan){
                Reward ret = new Reward();
                
                if(oRan.nextInt(MYTHICAL_DROP_RATE) == 0){
                    ret.recipe = "Phantom Striker";
                }
                
                return ret;
            }
        };
        new Monster("Howlitzer",FAMILY_UNDEAD,BASE_HEALTH*4);
        new Monster("Vile Howlitzer",FAMILY_UNDEAD, FAMILY_POISON,BASE_HEALTH*4);
        new Monster("Spookat",FAMILY_UNDEAD,BASE_HEALTH*3);
        new Monster("Grimalkat",FAMILY_UNDEAD,BASE_HEALTH*18);
        new Monster("Hurkat",FAMILY_UNDEAD,FAMILY_POISON,BASE_HEALTH*4){
            public Reward generateReward(GateChannel chan){
                Reward ret = super.generateReward(chan);
                
                if(oRan.nextInt(RARE_DROP_RATE) == 0){
                    ret.recipe = "Toxic Catalyzer";
                }
                
                return ret;
            }
        };
        new Monster("Deadnaut",FAMILY_UNDEAD,BASE_HEALTH*12);
        
        new Monster("Volcanic Whisp", FAMILY_FIRE, BASE_HEALTH*2);
        new Monster("Winter Whisp", FAMILY_FREEZE, BASE_HEALTH*2);
        new Monster("Bog Whisp", FAMILY_POISON, BASE_HEALTH*2);
        new Monster("Golden Bog Whisp", FAMILY_POISON, BASE_HEALTH*200);
        new Monster("Storm Whisp", FAMILY_SHOCK, BASE_HEALTH*2);
        
        new Monster("Snarbolax",FAMILY_BEAST,BASE_HEALTH*23);
        new Monster("Tortodrone",FAMILY_CONSTRUCT,BASE_HEALTH*23);
        new Monster("Maulos",FAMILY_FIEND,FAMILY_FREEZE, BASE_HEALTH*30);
        new Monster("Battlepod",FAMILY_CONSTRUCT,BASE_HEALTH*30);
    }
}
