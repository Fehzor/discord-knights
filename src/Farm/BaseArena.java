/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Farm;

import static Farm.BaseArea.monsterTiers;
import static Farm.Monster.FAMILY_BEAST;
import static Farm.Monster.FAMILY_CONSTRUCT;
import static Farm.Monster.FAMILY_FIEND;
import static Farm.Monster.FAMILY_FIRE;
import static Farm.Monster.FAMILY_FREEZE;
import static Farm.Monster.FAMILY_GREMLIN;
import static Farm.Monster.FAMILY_POISON;
import static Farm.Monster.FAMILY_SHOCK;
import static Farm.Monster.FAMILY_SLIME;
import static Farm.Monster.FAMILY_UNDEAD;
import static Farm.Monster.RARE_DROP_RATE;
import Loot.Reward;
import static Loot.SuperRandom.oRan;
import java.awt.Color;

/**
 *
 * @author FF6EB
 */
public class BaseArena extends BaseArea{
    
    public int fam;
    public int depth;
    
    public BaseArena(String name, int fam, int depth){
            super(name, fam, depth);
            this.fam = fam;
            this.gateTime = 60 * 1000 * 6 * depth;
            
            MG = new MonsterGenArena(this){
                public void setup(){
                    for(int i = 0; i < depth; ++i){
                    this.register(monsterTiers(fam,0),35);
                    this.register(monsterTiers(fam,1),15 + i);
                    this.register(monsterTiers(fam,2),8 + (int)Math.pow(i, 2));
                    this.register(monsterTiers(fam,3),3 + (int)Math.pow(i, 3));
                    this.register(monsterTiers(fam,4),0 + (int)Math.pow(i, 4));


                    this.maxKills = depth * 55;
                    this.boss = new String[]{monsterTiers(fam,5),monsterTiers(fam,5),monsterTiers(fam,5)};
                    }
                }
            };
            
            this.color = getColor().darker();
            
            this.minimumMineralAmount= (int)Math.pow(7, depth) - 1;
            this.mineralList=getMinerals(fam);
        }
        
        public void setPrize(Reward set){
            
            if(oRan.nextInt(RARE_DROP_RATE) == 0){
                set.gear = "Darkfang Shield";
            }
            
            switch(fam){
                case FAMILY_BEAST:
                    set.recipe = "Hunting Blade";
                    break;
                case FAMILY_SLIME:
                    set.recipe = "Cautery Sword";
                    break;
                case FAMILY_GREMLIN:
                    set.recipe = "Blackhawk";
                    break;
                case FAMILY_CONSTRUCT:
                    set.recipe = "Deconstructor";
                    break;
                case FAMILY_FIEND:
                    set.recipe = "Raptor";
                    break;
                case FAMILY_UNDEAD:
                    set.recipe = "Silversix";
                    break;
                case FAMILY_FIRE:
                    set.recipe = "Fiery Vaporizer";
                    break;
                case FAMILY_SHOCK:
                    set.recipe = "Static Capacitor";
                    break;
                case FAMILY_POISON:
                    set.recipe = "Toxic Vaporizer";
                    break;
                case FAMILY_FREEZE:
                    set.recipe = "Freezing Vaporizer";
                    break;
            }
        }
        
        public String[][] getMinerals(int family){
            String [][] ret = new String[24][4];
            String [] mins = new String[4];
            mins[3] = "Arena Alloy";
            
            switch(family){
                case FAMILY_BEAST:
                    mins[0] = "Beast Alloy";
                    mins[1] = "Red";
                    mins[2] = "Green";
                    break;
                case FAMILY_SLIME:
                    mins[0] = "Slime Alloy";
                    mins[1] = "Yellow";
                    mins[2] = "Green";
                    break;
                case FAMILY_GREMLIN:
                    mins[0] = "Gremlin Alloy";
                    mins[1] = "Red";
                    mins[2] = "Blue";
                    break;
                case FAMILY_CONSTRUCT:
                    mins[0] = "Construct Alloy";
                    mins[1] = "Blue";
                    mins[2] = "Yellow";
                    break;
                case FAMILY_FIEND:
                    mins[0] = "Fiend Alloy";
                    mins[1] = "Red";
                    mins[2] = "Purple";
                    break;
                case FAMILY_UNDEAD:
                    mins[0] = "Undead Alloy";
                    mins[1] = "Purple";
                    mins[2] = "Yellow";
                    break;
                case FAMILY_FIRE:
                    mins[0] = "Fire Alloy";
                    mins[1] = "Red";
                    mins[2] = "Yellow";
                    break;
                case FAMILY_SHOCK:
                    mins[0] = "Shock Alloy";
                    mins[1] = "Purple";
                    mins[2] = "Blue";
                    break;
                case FAMILY_POISON:
                    mins[0] = "Poison Alloy";
                    mins[1] = "Purple";
                    mins[2] = "Green";
                    break;
                case FAMILY_FREEZE:
                    mins[0] = "Freeze Alloy";
                    mins[1] = "Blue";
                    mins[2] = "Green";
                    break;
            }
            
            AreaList.permute(ret, mins);
            
            return ret;
        }
}
