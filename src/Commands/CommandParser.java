/*
 * Copyright (C) 2017 FF6EB4
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package Commands;


import Bot.Launcher;
import Commands.Implementation.CraftCommand;
import Commands.Implementation.EquipCommand;
import Commands.Implementation.Grant;
import Commands.Implementation.Inv;
import Commands.Implementation.Recolor;
import Commands.Implementation.Take;
import Commands.Implementation.ViewCommand;
import Commands.Implementation.WorldInfo;
import java.util.ArrayList;
import java.util.HashMap;
import sx.blah.discord.handle.obj.IChannel;

/**
 *
 * @author FF6EB4
 */
public class CommandParser {
    public HashMap<String,Command> commandList = new HashMap<>();
    
    public void parseCommand(String S, long ID){
        String[] split = S.split(" ",2);
        String signature = split[0];
        signature = signature.toLowerCase();
        
        String args = "";
        if(split.length > 1){
            args = split[1];
        }
        
        if(commandList.containsKey(signature.toLowerCase())){
            Command C = commandList.get(signature.toLowerCase());
            try{
                C.execute(args,ID);
            } catch (Exception E){
                E.printStackTrace();
            }
        }
    }
    
    public CommandParser(){
        addCommand(new Recolor());
        addCommand(new Inv());
        
        addCommand(new CraftCommand());
        addCommand(new ViewCommand());
        addCommand(new EquipCommand());
        
        //FOR NOW
        addCommand(new Grant());
        addCommand(new Take());
    }
    
    public void addCommand(Command com){
        for(String s : com.signature){
            this.commandList.put(s,com);
        }
    }
    
    
}
