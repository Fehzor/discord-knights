/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.Implementation;

import Bot.Launcher;
import Commands.Command;
import Gates.GateChannel;
import Gates.GateMessage;
import User.UserData;
import org.apache.commons.lang3.text.WordUtils;
import sx.blah.discord.handle.obj.IChannel;

/**
 *
 * @author FF6EB
 */
public class Grant extends Command{
    
    public Grant(){
        this.category = 0;
        this.signature = new String[]{"gimme"};
        this.description = "Deposits # color etc.";
    }
    
    public void execute(String params, long ID){
        UserData UD = UserData.getUD(ID);
        IChannel last = Launcher.client.getChannelByID(UD.lastChannel.getData());
        GateChannel gate = GateChannel.get(last);
        
        String[]parmbreak = params.split(" ",2);
        String ret = "";
        
        if(parmbreak[0].equals("recipe")){
            UD.addRecipe(parmbreak[1]);
            ret+="ADDED "+parmbreak[1];
        }
        
        if(parmbreak[0].equals("mineral")){
            UD.giveMinerals(parmbreak[1], 100);
            ret+="ADDED "+parmbreak[1];
        }
        
        if(parmbreak[0].equals("material")){
            UD.giveMaterials(parmbreak[1], 100);
            ret+="ADDED "+parmbreak[1];
        }
        
        if(parmbreak[0].equals("gear")){
            UD.give(parmbreak[1]);
            ret+="ADDED "+parmbreak[1];
        }
        
        new GateMessage("--GRANT--",ret).send(last);
    }
    

}
