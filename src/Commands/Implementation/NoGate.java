/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.Implementation;

import User.UserData;
import Commands.Command;
import Commands.CommandParser;
import Bot.Launcher;
import static Loot.SuperRandom.oRan;
import Gates.GateChannel;
import Gates.GateMessage;
import java.awt.Color;
import java.util.List;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IRole;

/**
 *
 * @author FF6EB4
 */
public class NoGate extends Command{
    
    public NoGate(){
        this.category = 0;
        this.signature = new String[]{"gate"};
        this.description = "Information regarding the gate (channel) is not displayed, because you are in a PM channel. Goober.";
    }
    
    public void execute(String params, long ID){
       UserData UD = UserData.getUD(ID);
       
       IChannel chan = Launcher.client.getChannelByID(UD.lastChannel.getData());
       
       new GateMessage("GATE","Right now you're in the DM channel, where commands like inv go. \nTry using gate in a gate channel- like mineral-quarry!").send(chan);
    }
}