/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.Implementation;

import Bot.Launcher;
import Commands.Command;
import Gates.GateChannel;
import Gates.GateMessage;
import Item.Parser.EffectParser;
import Item.Structure.CraftNode;
import User.UserData;
import org.json.JSONObject;
import sx.blah.discord.handle.obj.IChannel;

/**
 *
 * @author FF6EB
 */
public class Charge extends Command{
    
    public Charge(){
        this.category = 0;
        this.signature = new String[]{"charge"};
        this.description = "Release a charge attack!";
    }
    
    public void execute(String params, long ID){
        UserData UD = UserData.getUD(ID);
        
        String wep = "Bad";
        if(params.toUpperCase().equals("A")||params.equals("1")){
            wep = UD.wepA.getData();
        }
        if(params.toUpperCase().equals("B")||params.equals("2")){
            wep = UD.wepB.getData();
        }
        if(params.toUpperCase().equals("C")||params.equals("3")){
            wep = UD.wepC.getData();
        }
        if(wep.equals("Bad")){
            Launcher.send("Useage: charge [slot]; i.e. charge B or charge 3",Launcher.client.getChannelByID(UD.lastChannel.getData()));
            return;
        }
        
        if(UD.charges.getData() < CraftNode.getNode(wep).stars){
            Launcher.send("Gather charges to launch your assault- you have "+UD.charges.getData()+" out of "+CraftNode.getNode(wep).stars+" needed!",Launcher.client.getChannelByID(UD.lastChannel.getData()));
            return;
        }
        
        JSONObject charge = CraftNode.getNode(wep).charge;
        if(charge!=null){
            GateChannel chan = GateChannel.get(Launcher.client.getChannelByID(UD.lastChannel.getData()));
            EffectParser.parse(UD, chan, charge);
            UD.charges.append(-1 * CraftNode.getNode(wep).stars);
            Launcher.send("**--CHARGE SUCCESS--**",Launcher.client.getChannelByID(UD.lastChannel.getData()));
            return;
        } else {
            Launcher.send("This weapon has no charge attack! Beg Fehzor to add it!",Launcher.client.getChannelByID(UD.lastChannel.getData()));
            return;
        }
    }
}
