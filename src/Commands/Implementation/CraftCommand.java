/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.Implementation;

import Bot.Launcher;
import Commands.Command;
import Gates.GateChannel;
import Gates.GateMessage;
import Item.Craft;
import User.UserData;
import java.util.HashMap;
import org.apache.commons.lang3.text.WordUtils;
import sx.blah.discord.handle.obj.IChannel;

/**
 *
 * @author FF6EB
 */
public class CraftCommand extends Command{
    
    public CraftCommand(){
        this.category = 0;
        this.signature = new String[]{"craft"};
        this.description = "Craft something";
    }
    
    public void execute(String params, long ID){
        UserData UD = UserData.getUD(ID);
        IChannel last = Launcher.client.getChannelByID(UD.lastChannel.getData());
        GateChannel gate = GateChannel.get(last);
        
        String what = WordUtils.capitalizeFully(params);
        
        String ret = Craft.create(UD, what);
        
        new GateMessage("--CRAFT--",ret).send(last);
    }
}
