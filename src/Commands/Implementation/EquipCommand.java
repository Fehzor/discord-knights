/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.Implementation;

import Bot.Launcher;
import Commands.Command;
import Display.SingleView;
import Gates.GateChannel;
import Gates.GateMessage;
import Item.Craft;
import Item.Structure.CraftNode;
import User.UserData;
import java.util.HashMap;
import org.apache.commons.lang3.text.WordUtils;
import sx.blah.discord.handle.obj.IChannel;

/**
 *
 * @author FF6EB
 */
public class EquipCommand extends Command{
    
    public EquipCommand(){
        this.category = 0;
        this.signature = new String[]{"equip"};
        this.description = "Number, weapon";
    }
    
    public void execute(String params, long ID){
        UserData UD = UserData.getUD(ID);
        IChannel last = Launcher.client.getChannelByID(UD.lastChannel.getData());
        GateChannel gate = GateChannel.get(last);
        
        try{
        
            String slot = params.split(" ",2)[0];
            String what = WordUtils.capitalizeFully(params.split(" ",2)[1]);

            int slut = Integer.parseInt(slot);

            ((SingleView)UD.invObj.views.get("single")).equip(CraftNode.getNode(what),slut);
        } catch (Exception E){
            new GateMessage("EQUIP","Error equipping item- Syntax is equip # gear").send(last);
        }
    }
}
