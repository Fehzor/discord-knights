/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.Implementation;

import Bot.Launcher;
import Commands.Command;
import Gates.GateChannel;
import Gates.GateMessage;
import User.UserData;
import sx.blah.discord.handle.obj.IChannel;

/**
 *
 * @author FF6EB
 */
public class Inv extends Command{
    
    public Inv(){
        this.category = 0;
        this.signature = new String[]{"inv"};
        this.description = "Information regarding the world is displayed.";
    }
    
    public void execute(String params, long ID){
        UserData UD = UserData.getUD(ID);
        UD.invObj.topView();
    }
}
