/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.Implementation;

import Bot.Launcher;
import Commands.Command;
import Gates.GateChannel;
import Gates.GateMessage;
import User.UserData;
import java.util.HashMap;
import org.apache.commons.lang3.text.WordUtils;
import sx.blah.discord.handle.obj.IChannel;

/**
 *
 * @author FF6EB
 */
public class Deposit extends Command{
    
    public Deposit(){
        this.category = 0;
        this.signature = new String[]{"deposit"};
        this.description = "Deposits # color etc.";
    }
    
    public void execute(String params, long ID){
        UserData UD = UserData.getUD(ID);
        IChannel last = Launcher.client.getChannelByID(UD.lastChannel.getData());
        GateChannel gate = GateChannel.get(last);
        
        String [] parmbreak = params.split(" ");
        String ret = "";
        if (parmbreak.length == 0 || (parmbreak.length == 1 && parmbreak[0].equals("all"))) {
            HashMap<String, Integer> userMinerals = UD.minerals.getData();
            for (String color : userMinerals.keySet().toArray(new String[userMinerals.size()])) {
                ret+=deposit(UD, gate, userMinerals.get(color), color);
            }
        }
        else {
            int i = 0;
            int amt = -1;
            String color = "";
            
            while(i < parmbreak.length){
                if(parmbreak[i].equals("all")){
                    try{
                        amt = UD.minerals.getData().get(color.trim());
                        ret+=deposit(UD, gate, amt, color.trim());
                    } catch (Exception E){
                        ret+="Syntax error detected.\n";
                        i=parmbreak.length;
                    }
                    color = "";
                } else if(parmbreak[i].charAt(0) >= '0' && parmbreak[i].charAt(0) <= '9'){
                    try{
                        amt = Integer.parseInt(parmbreak[i]);
                        ret+=deposit(UD, gate, amt, color.trim());
                    } catch (Exception E){
                        ret+="Syntax error detected.\n";
                        i=parmbreak.length;
                    }
                    color = "";
                } else {
                    color += WordUtils.capitalizeFully(parmbreak[i])+" ";
                }
                ++i;
            }
        }
        
        new GateMessage("--DEPOSIT--",ret).send(last);
    }
    
    public String deposit(UserData UD, GateChannel GC, int amt, String color){
        if(UD.minerals.getData().containsKey(color) && UD.minerals.getData().get(color) >= amt){
            UD.takeMinerals(color, amt);
            if(GC.minerals.getData().containsKey(color)){
                GC.minerals.getData().put(color, GC.minerals.getData().get(color)+amt);
            } else {
                GC.minerals.getData().put(color, amt);
            }
            GC.minerals.write();
            return "Deposited "+amt+" "+color+" minerals..\n";
        } else {
            if(UD.minerals.getData().containsKey(color)){
                return "You only have "+UD.minerals.getData().get(color)+" "+color+" minerals ("+amt+" entered)\n";
            }
            return "You don't have ANY \""+color+"\" minerals. Check for typos?\n";
        }
    }
}
