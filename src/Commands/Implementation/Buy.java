/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.Implementation;

import Bot.Launcher;
import Commands.Command;
import Farm.Terminal;
import static Farm.Terminal.RECIPE_COST;
import Gates.GateChannel;
import Gates.GateMessage;
import User.UserData;
import java.util.ArrayList;
import org.apache.commons.lang3.text.WordUtils;
import sx.blah.discord.handle.obj.IChannel;

/**
 *
 * @author FF6EB
 */
public class Buy extends Command{
    
    public Buy(){
        this.category = 0;
        this.signature = new String[]{"buy"};
        this.description = "Buy something from basil.";
    }
    
    public void execute(String params, long ID){
        UserData UD = UserData.getUD(ID);
        IChannel last = Launcher.client.getChannelByID(UD.lastChannel.getData());
        GateChannel gate = GateChannel.get(last);
        
        String ret = "";
        
        ArrayList<String> recipes = Terminal.selling.getData();
        
        if(!gate.currentArea.name.equals("Clockwork Terminal")){
            new GateMessage("--BASIL--","Isn't here now.. \nBe patient after a non-quarry level and he'll show up..").send(last);
            return;
        }
        
        if(UD.crowns.getData() < RECIPE_COST){
            new GateMessage("--BASIL--","Recipe cost: "+RECIPE_COST).send(last);
            return;
        }
        
        if(recipes.contains(WordUtils.capitalizeFully(params.toLowerCase()))){
            UD.addRecipe(WordUtils.capitalizeFully(params.toLowerCase()));
            UD.crowns.append((long)(-1l * RECIPE_COST));
            new GateMessage("--BASIL--","Bought recipe for: "+params).send(last);
            return;
        }
        
        
    }
    

}
