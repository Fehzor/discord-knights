/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.Implementation;

import Bot.Launcher;
import Commands.Command;
import Gates.GateChannel;
import Gates.GateMessage;
import User.UserData;
import org.apache.commons.lang3.text.WordUtils;
import sx.blah.discord.handle.obj.IChannel;

/**
 *
 * @author FF6EB
 */
public class Take extends Command{
    
    public Take(){
        this.category = 0;
        this.signature = new String[]{"nomo"};
        this.description = "Deposits # color etc.";
    }
    
    public void execute(String params, long ID){
        UserData UD = UserData.getUD(ID);
        IChannel last = Launcher.client.getChannelByID(UD.lastChannel.getData());
        GateChannel gate = GateChannel.get(last);
        
        String[]parmbreak = params.split(" ",2);
        String ret = "";
        
        if(parmbreak[0].equals("recipe")){
            UD.recipes.getData().remove(parmbreak[1]);
            ret+="TAKED "+parmbreak[1];
        }
        
        if(parmbreak[0].equals("mineral")){
            UD.takeMinerals(parmbreak[1],UD.minerals.getData().get(parmbreak[1]));
            ret+="TAKED "+parmbreak[1];
        }
        
        if(parmbreak[0].equals("material")){
            UD.takeMaterials(parmbreak[1], UD.materials.getData().get(parmbreak[1]));
            ret+="TAKED "+parmbreak[1];
        }
        
        if(parmbreak[0].equals("gear")){
            UD.take(parmbreak[1]);
            ret+="TAKED "+parmbreak[1];
        }
        
        new GateMessage("--TAKE--",ret).send(last);
    }
    

}
