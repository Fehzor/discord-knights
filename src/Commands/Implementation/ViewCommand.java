/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.Implementation;

import Bot.Launcher;
import Commands.Command;
import Display.InventoryDisplay;
import Display.OrderView;
import Gates.GateChannel;
import Gates.GateMessage;
import Item.Craft;
import Item.Structure.CraftNode;
import User.UserData;
import java.util.HashMap;
import org.apache.commons.lang3.text.WordUtils;
import sx.blah.discord.handle.obj.IChannel;

/**
 *
 * @author FF6EB
 */
public class ViewCommand extends Command{
    
    public ViewCommand(){
        this.category = 0;
        this.signature = new String[]{"view"};
        this.description = "View something";
    }
    
    public void execute(String params, long ID){
        UserData UD = UserData.getUD(ID);
        IChannel last = Launcher.client.getChannelByID(UD.lastChannel.getData());
        GateChannel gate = GateChannel.get(last);
        
        String what = WordUtils.capitalizeFully(params);
        
        
        
        try{
            InventoryDisplay inventory = UD.invObj;
            CraftNode set = CraftNode.getNode(what);
            ((OrderView)inventory.views.get("order")).item = set;
            inventory.switchViews("order");
        } catch (Exception E){
            new GateMessage("--VIEW--","It broke! Try again!").send(last);
        }
        
        
    }
}
