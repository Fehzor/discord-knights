/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.Implementation;

import User.UserData;
import Commands.Command;
import Commands.CommandParser;
import Bot.Launcher;
import static Loot.SuperRandom.oRan;
import Gates.GateChannel;
import Gates.GateMessage;
import java.awt.Color;
import java.util.List;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IRole;

/**
 *
 * @author FF6EB4
 */
public class WorldInfo extends Command{
    
    public WorldInfo(){
        this.category = 0;
        this.signature = new String[]{"gate"};
        this.description = "Information regarding the gate (channel) is displayed.";
    }
    
    public void execute(String params, long ID){
       UserData UD = UserData.getUD(ID);
       
       IChannel chan = Launcher.client.getChannelByID(UD.lastChannel.getData());
       
       GateChannel wc = GateChannel.get(chan);
       
       GateMessage wm = wc.gateInfo();
       
       wm.send(chan);
    }
}